<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProjectRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    if ($this->method() == 'PUT') {
      return [
        'name' => Rule::unique('projects')->where('company_id', auth()->user()->companyUser->company_id)->ignore($this->route('project'))
      ];
    }


    return [
      'name' => Rule::unique('projects')->where('company_id', auth()->user()->companyUser->company_id)
    ];
  }
}
