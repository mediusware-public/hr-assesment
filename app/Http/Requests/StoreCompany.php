<?php

namespace App\Http\Requests;
use Illuminate\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreCompany extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
      if($this->id) {
        return [
          'name' => 'required',
          'phone' => 'required',
          'email' => [
            'required',
            'email',
            Rule::unique('companies', 'email')->ignore($this->id),
          ],
          'address' => 'required',
          'company_plan_id' => 'required',
          'owner_title'     => 'required',
          'owner_first_name'=>  'required',
          'owner_last_name' => 'required',
          'owner_phone' => 'required',
          'owner_email' => [
            'required',
            'email',
            Rule::unique('users', 'email')->ignore($request->owner_id)
          ],
          // 'owner_password' => 'required|min:6',
        ];
      } else {
        return [
          'name' => 'required',
          'phone' => 'required',
          'email' => 'required|email|unique:companies,email',
          'address' => 'required',
          'company_plan_id' => 'required',
          'owner_title'     => 'required',
          'owner_first_name'=>  'required',
          'owner_last_name' => 'required',
          'owner_phone' => 'required',
          'owner_email' => 'required|email|unique:users,email',
          'owner_password' => 'required|min:6',
        ];
      }
    }
}
