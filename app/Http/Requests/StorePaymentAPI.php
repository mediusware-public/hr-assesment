<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePaymentAPI extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->input('has_paypal') == true){
            return [
                'paypal_environment' => 'required',
                'paypal_customer_id' => 'required',
                'paypal_customer_secret' => 'required',
            ];
        } else {
            return [
                'stripe_environment' => 'required',
                'stripe_publisher_key' => 'required',
                'stripe_secret_key' => 'required',
                'stripe_webhook_signing_secret_key' => 'required',
            ];
        }
    }
}
