<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->input('has_google_drive') == true){
            return [
                'google_drive_developer_key' => 'required',
                'google_drive_client_id' => 'required',
                'google_drive_app_id' => 'required',
                'google_drive_scopes' => 'required',
            ];
        }
        else if($this->input('has_dropbox') == true){
            return [
                'dropbox_client_id' => 'required',
                'dropbox_client_secret' => 'required',
            ];
        }
        else if($this->input('has_one_drive') == true){
            return [
                'onedrive_client_id' => 'required',
                'onedrive_client_secret' => 'required',
            ];
        }
        else if($this->input('has_s3') == true){
            return [
                's3_client_id' => 'required',
                's3_client_secret' => 'required',
            ];
        }
        else
        {
            return [
                'fatture_api_client_id' => 'required',
                'fatture_api_client_secret' => 'required',
            ];
        }
    }
}
