<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreHoganApiCredentials extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//      $rules = [];
//
//      if ($this->attributes->has('some-key')) {
//        $rules['other-key'] = 'required|unique|etc';
//      }
//
//      if ($this->attributes->get('some-key') == 'some-value') {
//        $rules['my-key'] = 'in:a,b,c';
//      }
//
//      if ($this->attributes->get('some-key') == 'some-value') {
//        $this->attributes->set('key', 'value');
//      }
//
//      return $rules;

      return [
        'username'      => 'required',
        'clientid'     => 'required',
        'password'  => 'required',
      ];
    }
}
