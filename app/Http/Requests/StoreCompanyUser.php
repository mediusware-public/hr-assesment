<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreCompanyUser extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules(Request $request)
  {

    $rules = [
      'first_name' => 'required',
      'last_name' => 'required',
      'title' => 'required',
      'email' => 'required|email|unique:users,email',
      'password' => 'required',
      'phone' => 'required|min:6',
      'address' => 'required|max:190',
      'time_zone' => 'required',
      'role' => 'required',
    ];

    if ($this->method() == 'PUT') {
      $var = $this->route('user')?$this->route('user'):$this->route('hr');
      $rules['password']  = '';
      $rules['email'] = 'required|email|unique:users,email,' . $var;
    }

    return $rules;
  }
}
