<?php

namespace App\Http\Controllers;
use App\Helpers\HoganHelper;
use App\Http\Requests\ProjectRequest;
use App\Models\Company\CompanyUser;
use App\Models\Company\Project;
use App\Models\Hogan\Hogan;

use Illuminate\Http\Request;
use App\Http\Requests\StoreTicket;
use App\Exports\TicketsExport;
use Illuminate\Support\Facades\Http;

use App\Services\TicketService;

use App\Models\Tickets\Ticket;
use App\Models\Tickets\TicketFile;
use App\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use PDF;
use Maatwebsite\Excel\Facades\Excel;

class ProjectController extends Controller
{

  public function __construct()
  {
    $this->credentials = [
      'grant_type' => 'password',
      'username' => 'HuMAllzuser01',
      'password' => 'Hog@n154!',
      'clientid' => 'ALLEANZAMC'
    ];
    $this->ticketItems = 1;
  }


  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    //Pagination Items
    $items = request()->items ? request()->items : $this->ticketItems;
    $projects = Project::where('company_id', Auth()->user()->companyUser->company_id)->paginate(20);

    //get all projects
    return response()->json($projects);

  }


  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(ProjectRequest $request)
  {
    $exist_project = Project::where('name', $request->groupName)->where('company_id', auth()->user()->companyUser->company_id)->first();

    if(!$exist_project) {
      $tokenInfo = HoganHelper::getAccessToken();
      if(array_key_exists("error",$tokenInfo->responseData))
      {
        return response()->json(['message' => $tokenInfo->responseData['error_description']], 400);
      } else {
        try {
          $response = Http::withToken($tokenInfo->responseData['access_token'])->post("$tokenInfo->url/Clients/". $tokenInfo->halo_clientid . '/Groups/', [
            'clientUserId' => $tokenInfo->halo_clientid,
            'groupName' => $request->groupName
          ]);

          $project = Project::create([
            'company_id' => auth()->user()->companyUser->company_id,
            'user_id' => auth()->user()->id,
            'slug' => Str::slug($request->groupName),
            'name' => $request->groupName,
          ]);
          return response()->json($project);
        } catch (\Exception $e) {
          return response()->json(['message' => $e->getMessage() ], 400);
        }
      }
    } else {
      return response()->json(['message' => 'Project Already Exists.'], 400);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function show($slug)
  {
    $project = Project::query()->where('slug', $slug)->where('company_id', auth()->user()->companyUser->company_id)->first();
    return response()->json($project);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function deleteFile($id)
  {

  }

}
