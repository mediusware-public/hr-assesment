<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreHoganApiCredentials;
use App\Models\Hogan\Hogan;
use Illuminate\Http\Request;

class HoganController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

    //Get Payment
    $hogan = Hogan::MyCompany()->first();

    if ($hogan) {
      return response()->json($hogan);
    } else {
      $hoganApiData = (object)[
        'active' => 0,
        'production_link' => null,
        'production_halo_client_id' => null,
        'production_client_id' => null,
        'production_user_id' => null,
        'production_password' => null,
        'development_link' => null,
        'development_halo_client_id' => null,
        'development_client_id' => null,
        'development_user_id' => null,
        'development_password' => null,

      ];
      return response()->json($hoganApiData);
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $company_id = isset(auth()->user()->companyUser) ? auth()->user()->companyUser->company_id : 0;
    $hogan = Hogan::firstOrNew(array('company_id' => $company_id));
    $hogan->fill($request->all());
    $hogan->company_id = $company_id;
    $hogan->created_by = auth()->user()->id;
    $hogan->active = isset($request->active) && $request->active == true ? 1 : 0;
    $hogan->save();
    return response()->json($hogan);
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }
}
