<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\PasswordResetJob;
use App\Models\Company\CompanyUser;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Jenssegers\Agent\Agent;
use Lcobucci\JWT\Parser;
use App\Notifications\SendLoginAsOTP;
use App\User;
use App\PasswordReset;
use App\Models\LoginAsOtp;

class AuthController extends Controller
{
  public function login(Request $request)
  {
    $request->validate([
      'email' => 'required|string|email',
      'password' => 'required|string',
      'remember_me' => 'boolean'
    ]);

    $credentials = request(['email', 'password']);

    if (!Auth::attempt($credentials))
      return response()->json([
        'message' => 'Unauthorized'
      ], 401);

    $user = $request->user();

    $agent = new Agent();
    $tokenResult = $user->createToken("{$agent->platform()} - {$agent->browser()}");
    $token = $tokenResult->token;
    $token->ip = \Request::ip();
    if ($request->remember_me) {
      $token->expires_at = Carbon::now()->addWeeks(1);
    }

    $token->save();

    return response()->json([
      'access_token' => $tokenResult->accessToken,
      'token_type' => 'Bearer',
      'expires_at' => Carbon::parse(
        $tokenResult->token->expires_at
      )->toDateTimeString()
    ]);
  }

  public function resetPassword(Request $request)
  {
    if($request->token) $passwordReset = PasswordReset::where('token', $request->token)->orderBy('id', 'desc')->first();
    if($request->email) $passwordReset = PasswordReset::where('email', $request->email)->orderBy('id', 'desc')->first();

    if($passwordReset) {
      $user = User::where('email', $passwordReset->email);
      PasswordReset::where('email', $user->first()->email)->delete();
      $user->update(['password' => Hash::make($request->password)]);

      return response()->json(['status' => true, 'message' => 'Password changed successfully!']);
    }
    else {
      return response()->json(['status' => false, 'message' => 'Something went wrong, resend code!']);
    }

  }

  public function checkCode(Request $request)
  {
    if($request->token) $passwordReset = PasswordReset::where('token', $request->token)->orderBy('id', 'desc')->first();
    if($request->email) $passwordReset = PasswordReset::where('email', $request->email)->orderBy('id', 'desc')->first();

    $match = $passwordReset->code == $request->code;

    return response()->json($match);
  }

  public function checkToken(Request $request, $code_token)
  {
    $passwordReset = PasswordReset::where('token', $code_token)->first(['token', 'email']);
    return response()->json(['status' => $passwordReset != null, 'email' => $passwordReset->email]);
  }

  public function sendCode(Request $request)
  {
    $user = User::where('email', $request->email)->first();
    if(!$user) return response()->json(['message' => 'Wrong email address!'], 404);
    else {
      $code  = rand(111111, 999999);
      $email = $user->email;
      $token = encrypt($code.'___'.$email);
      $data = [
        'code'  => $code,
        'token' => $token,
        'email' => $email
      ];
      PasswordReset::where('email', $email)->delete();
      PasswordReset::create($data);
      $data['user'] = $user;
      dispatch(new PasswordResetJob($data));
      return response()->json('send!');
    }
  }

  public function loginAsOTP(Request $request, $id)
  {
    $user = User::find($id);
    $token = rand(11111, 99999);
    $email = $user->email;

    $mail = [
      'mail_subject' => 'Login OTP',
      'token'        => $token,
      'time'         => Carbon::now(),
      'user'         => $user
    ];

    LoginAsOtp::where('email', $user->email)->delete();
    LoginAsOtp::create([
      'email' => $email,
      'token' => $token
    ]);

    Notification::route('mail', $email)->notify(new SendLoginAsOTP($mail));
    return response()->json($mail);
  }

  public function loginAs(Request $request, $id)
  {
    $value = $request->bearerToken();
    $oldId = (new Parser())->parse($value)->getClaim('jti');

    $token = $request->user()->tokens->find($oldId);
    $token->revoke();

    $user = User::find($id);

    $agent = new Agent();
    $tokenResult = $user->createToken("{$agent->platform()} - {$agent->browser()}");
    $token = $tokenResult->token;
    $token->ip = \Request::ip();

    $token->save();

    return response()->json([
      'access_token' => $tokenResult->accessToken,
      'token_type'   => 'Bearer',
      'user'         => $user,
      'expires_at'   => Carbon::parse(
        $tokenResult->token->expires_at
      )->toDateTimeString()
    ]);
  }

  public function getAuthUser(Request $request)
  {
    return $request->user();
  }

  public function logout(Request $request)
  {
    $value = $request->bearerToken();
    $id = (new Parser())->parse($value)->getClaim('jti');
    $token = $request->user()->tokens->find($id);
    $token->revoke();
    return response('Logout', 200);
  }

  public function loginHistory()
  {
    $history = DB::table('oauth_access_tokens')->where('user_id', auth()->user()->id)
      ->where('revoked', 0)
      ->orderBy('created_at', 'desc')->paginate(20);
    return response()->json($history);
  }

  public function loginHistoryByUserId($id)
  {
    $history = DB::table('oauth_access_tokens')->where('user_id', $id)
      ->orderBy('created_at', 'desc')->paginate(10);
    return response()->json($history);
  }

  public function loginHistoryByCompanyId($id)
  {
//    $users = DB::table('users')->select('first_name', 'email as user_email')->get();
//    $users = DB::table('users')
//      ->join('company_users', 'users.id', '=', 'company_users.user_id')
//      ->select('users.*', 'company_users.company_id', 'company_users.receive_communication')
//      ->get();
//    return response()->json($users);

    $query = CompanyUser::where('company_id', $id)->pluck('user_id');

    $history = DB::table('oauth_access_tokens')->whereIn('user_id', $query)
      ->leftJoin('users', 'oauth_access_tokens.user_id', '=', 'users.id')
      ->orderBy('oauth_access_tokens.created_at', 'desc')
      ->orderBy('oauth_access_tokens.revoked', 'asc')
      ->select(
        'oauth_access_tokens.*',
        'users.title as user_title',
        'users.first_name as user_first_name',
        'users.last_name as user_last_name',
        'users.email as user_email'
      )
      ->paginate(20);
    return response()->json($history);
  }

  public function logOutFromDevice(Request $request, $id)
  {
//    $token = $request->user()->tokens->find($id);
//    $token->revoke();
    DB::table('oauth_access_tokens')->where('id', $id)->update(['revoked' => 1]);
    return response('Logout', 200);
  }

  public function showResetForm(Request $request)
  {
    if (!empty($request->token)) {
      $user = \App\User::where('remember_token', $request->token)->first();
      if (!empty($user)) {
        return response()->json($user);
      } else {
        return response()->json('Something wrong');
      }
    } else {
      return response()->json('Something wrong');
    }

  }

  public function reset(Request $request)
  {
    $request->validate([
      'email' => 'required|string|email',
      'password' => ['required', 'string', 'confirmed'],
    ]);
    $user = \App\User::where('remember_token', $request->token)->first();
    $user->password = Hash::make($request->password);
    $user->remember_token = null;
    $user->update();

    $credentials = request(['email', 'password']);

    if (!Auth::attempt($credentials))
      return response()->json([
        'message' => 'Unauthorized'
      ], 401);

    $user = $request->user();

    $agent = new Agent();
    $tokenResult = $user->createToken("{$agent->platform()} - {$agent->browser()}");
    $token = $tokenResult->token;
    $token->ip = \Request::ip();
    if ($request->remember_me) {
      $token->expires_at = Carbon::now()->addWeeks(1);
    }

    $token->save();

    return response()->json([
      'access_token' => $tokenResult->accessToken,
      'token_type' => 'Bearer',
      'expires_at' => Carbon::parse(
        $tokenResult->token->expires_at
      )->toDateTimeString()
    ]);
  }
}
