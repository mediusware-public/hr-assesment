<?php

namespace App\Http\Controllers;

use App\Models\Company\CompanyUser;
use App\Models\Company\CompanyAssessment;

use App\Models\Company\ParticipantAssessment;

use App\Models\Company\Assessment;


use App\Models\Hogan\Hogan;
use Illuminate\Http\Request;
use App\Http\Requests\StoreTicket;
use App\Exports\TicketsExport;
use Illuminate\Support\Facades\Http;

use App\Services\TicketService;

use App\Models\Tickets\Ticket;
use App\Models\Tickets\TicketFile;
use App\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use PDF;
use Maatwebsite\Excel\Facades\Excel;

class CompanyAssessmentController extends Controller
{

    public function __construct()
    {
      $this->items = 10;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Pagination Items
        $items = request()->items ? request()->items:$this->items;

        //get all projects
        return response()->json(CompanyAssessment::paginate(100));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = $request->except(['company_id', 'project_id']);
      CompanyAssessment::where('company_id', request()->company_id)->where('project_id', request()->project_id)->delete();
      $companyAssessment = CompanyAssessment::insert($data);
      return $companyAssessment;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getProjectAssessments($company_id, $project_id)
    {
      $projectAssessment = [];
      $companyAssessment = CompanyAssessment::where('company_id', $company_id)->where('project_id', $project_id)->with('assessment:id,key,value')->get();
      foreach($companyAssessment as $assessment) {
        $projectAssessment[] = $assessment->assessment;
      }
      return response()->json($projectAssessment);
    }

    public function getAllAssessments()
    {
      $assessments = Assessment::query()->get(['id', 'key', 'value']);
      return response()->json($assessments);
    }

    public function show($slug)
    {
      $company_user = CompanyUser::where('user_id', auth()->user()->id)->first();
      $project = CompanyAssessment::query()->where('slug', $slug)->where('company_id', $company_user->company_id)->first();
      return response()->json($project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteFile($id)
    {

    }






  public function assignParticipantAssessment(Request $request)
  {
    ParticipantAssessment::where('participant_id', request()->participant_id)->delete();
    if(count($request->assessments) > 0) {
      foreach ($request->assessments as $key => $assessment) {
        ParticipantAssessment::create([
          'assessment_id' => $assessment,
          'participant_id' => request()->participant_id,
        ]);
      }
    }
    return response()->json(['message'=>'Successful'], 200);
  }

  public  function assignParticipantAssessmentBycheckboxSelect(Request $request) {
    $user_id = [];
    foreach ($request->all() as $key => $user) {
      array_push($user_id, $user['id']);
    }
    ParticipantAssessment::whereIn('participant_id', $user_id)->delete();

    foreach ($request->all() as $key => $user) {
      foreach ($user['newArray'] as $innerKey => $assessment) {
        ParticipantAssessment::create([
          'assessment_id' => $assessment,
          'participant_id' => $user['id'],
        ]);
      }
    }

    return response()->json(['message'=>'Successful'], 200);
  }

  public function getParticipantAssessment(Request $request)
  {
    $participantAssessments = ParticipantAssessment::where('participant_id', $request->participant_id)->with('assessment')->get();
    return response()->json($participantAssessments);
  }





  public function gotoParticipantTesturl(Request $request)
  {
    $hogan_credentials = Hogan::where('company_id', auth()->user()->companyUser->company_id)->first();
    //Hogan Test Url
    $hogan_username = $hogan_credentials['development_user_id'];
    $hogan_password = $hogan_credentials['development_password'];
    $hogan_participant_id = auth()->user()->participantId;
    $participant_firstName = auth()->user()->name;
    $participant_lastName = auth()->user()->name;
    $participant_languageId = 'en';
    $participant_assessmentId = $request->key;
    $return_url = 'http://localhost:8000/';

    $assessment_test_url = 'https://www.hoganuat.com/HasStandardApi/';

    $response = Http::asForm()->post($assessment_test_url, [
      'UserID' => $hogan_username,
      'Password' => $hogan_password,
      'UniqueID' => $hogan_participant_id,
      'LastName' => $participant_firstName,
      'FirstName' => $participant_firstName,
      'LanguageID' => $participant_languageId,
      'DirectAssessmentID' => $participant_assessmentId,
      'DisplayInformedConsent' => 'YES',
      'ReturnURL' => $return_url,
    ]);
  }
}
