<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Settings\Storage;
use App\Http\Requests\StorageRequest;
use Illuminate\Support\Facades\Auth;

class StorageApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get Storage
        $storage = Storage::first();
        if($storage) {
            return response()->json($storage, 200);
        } else {
            $storagesApiData = (object) [
                "google_drive_developer_key" => "",
                "google_drive_client_id" => "",
                "google_drive_app_id" => "",
                "google_drive_scopes" => "",
                "dropbox_client_id" => "",
                "dropbox_client_secret" => "",
                "onedrive_client_id" => "",
                "onedrive_client_secret" => "",
                "s3_client_id" => "",
                "s3_client_secret" => "",
                "fatture_api_client_id" => "",
                "fatture_api_client_secret" => "",
                "has_google_drive" => true,
                "has_dropbox" => false,
                "has_one_drive" => false,
                "has_s3" => false,
                "has_fatture_api" => false,
                "updated_by" => 1
            ];
            return response()->json(["data"=> $storagesApiData], 200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorageRequest $request)
    {
        //validate data with form requested
        $validated = $request->validated();

        $storage = Storage::firstOrNew();
        $storage->fill($request->all());
        $storage->updated_by = auth()->user()->id;
        $storage->save();
        return response()->json(["data"=> $storage], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($storage_type)
    {
        if($storage_type == 'googledrive')
        {
            $record = Storage::where('storage_type',$storage_type)->get();
            if(count($record) == 0)
            {
                $data = [];
                $data['storage_type'] = $storage_type;
                $data['developer_key'] = '';
                $data['client_id'] = '';
                $data['app_id'] = '';
                $data['scopes'] = '';
                return response()->json(["data"=> $data], 200);
            }
            else
            {
                $data = [];
                $data['storage_type'] = $record[0]->storage_type;
                $data['developer_key'] = $record[0]->developer_key;
                $data['client_id'] = $record[0]->client_id;
                $data['app_id'] = $record[0]->app_id;
                $data['scopes'] = $record[0]->scopes;
                return response()->json(["data"=> $data], 200);
            }
        }
        else if($storage_type == 'dropbox')
        {
            $record = Storage::where('storage_type',$storage_type)->get();
            if(count($record) == 0)
            {
                $data = [];
                $data['storage_type'] = $storage_type;
                $data['client_id'] = '';
                $data['client_secret'] = '';
                return response()->json(["data"=> $data], 200);
            }
            else
            {
                $data = [];
                $data['storage_type'] = $record[0]->storage_type;
                $data['client_id'] = $record[0]->client_id;
                $data['client_secret'] = $record[0]->client_secret;
                return response()->json(["data"=> $data], 200);
            }
        }
        else if($storage_type == 'onedrive')
        {
            $record = Storage::where('storage_type',$storage_type)->get();
            if(count($record) == 0)
            {
                $data = [];
                $data['storage_type'] = $storage_type;
                $data['client_id'] = '';
                $data['client_secret'] = '';
                $data['scopes'] = '';
                return response()->json(["data"=> $data], 200);
            }
            else
            {
                $data = [];
                $data['storage_type'] = $record[0]->storage_type;
                $data['client_id'] = $record[0]->client_id;
                $data['client_secret'] = $record[0]->client_secret;
                $data['scopes'] = $record[0]->scopes;
                return response()->json(["data"=> $data], 200);
            }
        }
        else if($storage_type == 's3drive')
        {
            $record = Storage::where('storage_type',$storage_type)->get();
            if(count($record) == 0)
            {
                $data = [];
                $data['storage_type'] = $storage_type;
                $data['developer_key'] = '';
                $data['client_id'] = '';
                $data['app_id'] = '';
                $data['scopes'] = '';
                return response()->json(["data"=> $data], 200);
            }
            else
            {
                $data = [];
                $data['storage_type'] = $record[0]->storage_type;
                $data['developer_key'] = $record[0]->developer_key;
                $data['client_id'] = $record[0]->client_id;
                $data['app_id'] = $record[0]->app_id;
                $data['scopes'] = $record[0]->scopes;
                return response()->json(["data"=> $data], 200);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
