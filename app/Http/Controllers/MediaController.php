<?php

namespace App\Http\Controllers;

use App\Models\File\Media;
use App\Services\CloudFilePicker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class MediaController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $company_id = isset(auth()->user()->companyUser->company_id) ? auth()->user()->companyUser->company_id : auth()->user()->id;
    $query = Media::query();
    if ($request->has('uploader_id') && $request->uploader_id != "undefined") {
      $query->where('user_id', $request->uploader_id);
    }
    if (Auth()->user()->role != "super_admin") {
      $query->where('company_id', $company_id);
    }
    if ($request->has('file_search')) {
      $query->where('title', 'like', '%' . $request->file_search . '%');
    }
    return response()->json($query->latest()->paginate(25));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

    try {
      if ($request->has('uploader_id') && $request->uploader_id != "undefined") {
        $uploader_id = $request->uploader_id;
      } else {
        $uploader_id = auth()->user()->id;
      }
      $company_id = isset(auth()->user()->companyUser->company_id) ? auth()->user()->companyUser->company_id : auth()->user()->id;

      if ($request->hasFile('file')) {
        foreach ($request->file('file') as $file) {
          $filenameWithExt = $file->getClientOriginalName();
//          $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
          $extension = $file->getClientOriginalExtension();
          $filetype = $file->getMimeType();
          $fileNameToStore = Str::uuid() . '_' . time() . '.' . $extension;
          $path = $file->storeAs('upload', $fileNameToStore, 'public');
          $file_url = "storage/" . $path;

          $fileUpload = new Media();
          $fileUpload->alt = '';
          $fileUpload->title = $filenameWithExt;
          $fileUpload->path = $file_url;
          $fileUpload->type = $filetype;
          $fileUpload->caption = '';
          $fileUpload->description = '';
          $fileUpload->slug = Str::slug($filenameWithExt, '-');
          $fileUpload->user_id = $uploader_id;
          $fileUpload->modified_by = $uploader_id;
          $fileUpload->company_id = $company_id;
          $fileUpload->save();
        }
        return response()->json(['status' => 'success', 'message' => 'File Upload Successfully']);
      }
    } catch (\Exception $e) {
      return response()->json(['status' => 'error', 'message' => $e->getMessage()]);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param \App\Media $media
   * @return \Illuminate\Http\Response
   */
  public function show(Media $media)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param \App\Media $media
   * @return \Illuminate\Http\Response
   */
  public function edit(Media $media)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param \App\Media $media
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $updateFile = Media::findorFail($request->id);
    $updateFile->alt = $request->alt;
    $updateFile->caption = $request->caption;
    $updateFile->description = $request->description;
    $updateFile->is_private = $request->is_private == true ? 1 : 0;
    $updateFile->save();
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\Media $media
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    /* unlink previous file */
    $deleteFile = Media::find($id);
    $file = $deleteFile->title;
    unlink(storage_path('app/public/upload/' . $file));

    /* delete parmanent file record */
    Media::where('id', $id)->delete();
  }

  public function selectMedia($id)
  {
    $selectedFile = Media::where('id', $id)->get();
    return response()->json($selectedFile[0]);
  }

  public function cloudFilePicker(Request $request)
  {
    if ($request->type == 'dropbox') {
      $file = (new CloudFilePicker($request))->getDropBoxFile();
    } elseif ($request->type == 'google-drive') {
      $file = (new CloudFilePicker($request))->getGoogleDriveFile();
    }  elseif ($request->type == 'one-drive') {
      $file = (new CloudFilePicker($request))->getOneDriveFile();
    }
    return response()->json([
      'data' => $file,
    ], 200);
  }

}
