<?php

namespace App\Http\Controllers;

use App\Exports\ParticipantCommunicationExport;
use App\Exports\SuperadminCompanyCommunicationExport;
use App\Exports\CompanyProjectCommunicationExport;
use App\Jobs\SendEmailJob;
use App\Models\Company\Project;
use App\Notifications\MailSendNotify;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\StoreMail;
use App\Models\Mail\Mail;
use App\Models\Company\Company;
use App\Notifications\NewPostNotify;
use Illuminate\Support\Facades\Notification;
use Illuminate\Notifications\Notifiable;

class MailController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //Sent Mails
    $sentMails = Mail::where('mail_sent', 1)->with('user')->paginate(10);
    return response()->json($sentMails);
  }

  public function inboxMail()
  {
    //Inbox Mails
    $inboxMails = Mail::where('mail_to', auth()->user()->email)->with('user')->paginate(10);
    return response()->json($inboxMails);
  }

  public function getParticipantCommunicationHistory(Request $request)
  {
    $participant = User::find($request->participant_id);
    //Sent Mails
    $participantEmails = Mail::where('mail_from', Auth()->user()->email)->where('mail_to', $participant->email)->with('user')->paginate(10);
    return response()->json($participantEmails);
  }


  public function getCompanyProjectCommunicationHistory(Request $request)
  {
    $project = Project::query()->where('slug', $request->project_slug)->where('company_id', auth()->user()->companyUser->company_id)->first();
    //Sent Mails
    $companyOwnerCommunications = Mail::where('company_id', auth()->user()->companyUser->company_id)->where('project_id', $project->id)->with('user')->paginate(10);
    return response()->json($companyOwnerCommunications);
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreMail $request)
  {
    //Participant Data
    if ($request->participant_id) {
      $participant = User::find($request->participant_id);
      $full_name = $participant->name;
      $address = $participant->address;
      $request->mail_body = str_replace('{{ $full_name }}', $full_name, $request->mail_body);
      $request->mail_body = str_replace('{{ $address }}', $address, $request->mail_body);
    }

    $project_id = null;
    $company_id = null;
    if ($request->project_slug) {
      $company_id = auth()->user()->companyUser->company_id;
      $project = Project::query()->where('slug', $request->project_slug)->where('company_id', $company_id)->first();
      $project_id = $project->id;
    }

    if ($request->companyUserId) {
      $user = User::where('id', $request->companyUserId)->with('companyUser')->first();
      $company_id = $user->companyUser->company_id;
    }

    $mail = new Mail();
    $mail->fill($request->all());
    $mail->mail_subject = $request->mail_subject;;
    $mail->mail_body = $request->mail_body;
    $mail->mail_attachment = $request->mail_attachment;
    $mail->user_id = auth()->user()->id;
    $mail->project_id = $project_id;
    $mail->company_id = $company_id;
    $mail->save();

    if (!$request->schedule_time) {
      $mail->mail_sent = 1;
      $mail->schedule_time = Carbon::now();
      $mail->save();
      //Sending mail to email
      dispatch(new SendEmailJob($mail));
    } else {
      $mail->mail_sent = 0;
      $mail->schedule_time = $request->schedule_time;
      $mail->save();
    }

    return response()->json($mail);
  }

  public function sendMultipleParticipantEmail(Request $request)
  {

    try {
      $mails = $request->mail_to;
      if (!empty($mails)) {
        $company_id = auth()->user()->companyUser->company_id;
        $project = Project::query()->where('slug', $request->project_slug)->where('company_id', $company_id)->first();
        $project_id = $project->id;
        foreach ($mails as $mailto) {
          $mail = new Mail();
          $mail->mail_to = $mailto;
          $mail->mail_from = auth()->user()->email;
          $mail->mail_subject = $request->mail_subject;
          $mail->mail_body = $request->mail_body;
          $mail->mail_attachment = $request->mail_attachment;
          $mail->project_id = $project_id;
          $mail->company_id = $company_id;
          $mail->user_id = auth()->user()->id;
          $mail->save();

          if(!$request->schedule_time) {
            $mail->mail_sent = 1;
            $mail->schedule_time = Carbon::now();
            $mail->save();
            //Sending mail to email
            dispatch(new SendEmailJob($mail));
          } else {
            $mail->mail_sent = 0;
            $mail->schedule_time = $request->schedule_time;
            $mail->save();
          }
        }
        return response()->json(['status' => 'success', 'message' => 'Successfully Send Email.']);
      }
      return response()->json(['status' => 'error', 'message' => 'Something Went Wrong Sending Email.'], 400);
    } catch (\Exception $e) {
      return response()->json(['status' => 'error', 'message' => $e->getMessage()], 400);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }

  public function exportParticipantCommunicationPdf(Request $request)
  {
    $participant = User::find($request->participant_id);
    $data['communications'] = Mail::where('mail_from', auth()->user()->email)->where('mail_to', $participant->email)->with('user')->get();
    $fileName = "file_" . rand(000000, 999999) . ".pdf";
    $path = storage_path('app/public/export-pdf/') . $fileName;
    PDF::loadView('export-pdf.communication-list', $data)->save($path);
    return response()->json(asset("storage/export-pdf/" . $fileName));
  }

  //Export Excel Company Data
  public function exportParticipantCommunicationExcel(Request $request)
  {
    $file_name = rand(1000, 999999) . ".xlsx";
    Excel::store(new ParticipantCommunicationExport($request), "export-excel/" . $file_name, 'public');
    return response()->json(asset("storage/export-excel/" . $file_name));
  }

  public function getSuperadminCompanyCommunicationList(Request $request)
  {
    //Sent Mails
    $companyCommunications = Mail::where('company_id', $request->company_id)->with('user')->paginate(10);
    return response()->json($companyCommunications);
  }

  public function exportSuperadminCompanyCommunicationListPdf(Request $request)
  {
    //Sent Mails
    $data['communications'] = Mail::where('company_id', $request->company_id)->with('user')->get();
    $fileName = "file_" . rand(000000, 999999) . ".pdf";
    $path = storage_path('app/public/export-pdf/') . $fileName;
    PDF::loadView('export-pdf.communication-list', $data)->save($path);
    return response()->json(asset("storage/export-pdf/" . $fileName));
  }

  //Export Excel Company Data
  public function exportSuperadminCompanyCommunicationListExcel(Request $request)
  {
    $file_name = rand(1000, 999999) . ".xlsx";
    Excel::store(new SuperadminCompanyCommunicationExport($request), "export-excel/" . $file_name, 'public');
    return response()->json(asset("storage/export-excel/" . $file_name));
  }


  public function exportCompanyProjectCommunicationHistoryPdf(Request $request)
  {
    $project = Project::query()->where('slug', $request->project_slug)->where('company_id', auth()->user()->companyUser->company_id)->first();
    //Sent Mails
    $data['communications'] = Mail::where('company_id', auth()->user()->companyUser->company_id)->where('project_id', $project->id)->with('user')->get();
    $fileName = "file_" . rand(000000, 999999) . ".pdf";
    $path = storage_path('app/public/export-pdf/') . $fileName;
    PDF::loadView('export-pdf.communication-list', $data)->save($path);
    return response()->json(asset("storage/export-pdf/" . $fileName));
  }

  //Export Excel Company Data
  public function exportCompanyProjectCommunicationHistoryExcel(Request $request)
  {
    $file_name = rand(1000, 999999) . ".xlsx";
    Excel::store(new CompanyProjectCommunicationExport($request), "export-excel/" . $file_name, 'public');
    return response()->json(asset("storage/export-excel/" . $file_name));
  }
}
