<?php

namespace App\Http\Controllers;

use App\Models\Membership\Invoice;
use Illuminate\Http\Request;

class CustomPaymentController extends Controller
{
  public function __construct()
  {
    $this->invoiceItems = 5;
  }

  public function index(Request $request)
  {
    //Pagination Items
    $items = request()->items ? request()->items:$this->invoiceItems;

    //get all tickets
    $invoices = Invoice::with('company')->with('user')->paginate($items);
    return response()->json($invoices);
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $invoice= new Invoice();
      $invoice->fill($request->all());
      $invoice->created_by = auth()->user()->id;
      $invoice->save();
      return response()->json($invoice);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
