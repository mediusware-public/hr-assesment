<?php

namespace App\Http\Controllers;

use App\Models\Company\Company;
use App\Models\Tickets\Ticket;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
  public function index()
  {
    $last_two_day = Carbon::today()->subDays(2);
    $data = [];
    if (auth()->user()->role == "admin") {
      $company = User::where('id', Auth()->user()->id)->with('companyUser')->first()->toArray();

      $users = User::query();
      if (!empty($company['company_user']['company']['id'])) {
        $users->whereHas('companyUser', function ($q) use ($company) {
          $q->where('company_id', $company['company_user']['company']['id']);
        });
      }
      $data['total_users'] = $users->count();
//      dd($data['total_users']);
//      $data['ticket'] = [
//        'total_ticket' => Ticket::count(),
//        'new_ticket' => Ticket::where('created_at', '>=', $last_two_day)->count(),
//        'open_ticket' => Ticket::where('status', 'close')->count(),
//        'complete_ticket' => Ticket::count() != 0 ? (Ticket::where('status', 'close')->count() * 100) / Ticket::count() : 0
//      ];
    } elseif (auth()->user()->role == 'user') {
      $data['total_company'] = Company::count();
      $data['ticket'] = [
        'total_ticket' => Ticket::count(),
        'new_ticket' => Ticket::where('created_at', '>=', $last_two_day)->count(),
        'open_ticket' => Ticket::where('status', 'close')->count(),
        'complete_ticket' => Ticket::count() != 0 ? (Ticket::where('status', 'close')->count() * 100) / Ticket::count() : 0
      ];
    } else {
      $data['total_company'] = Company::count();
      $data['ticket'] = [
        'total_ticket' => Ticket::count(),
        'new_ticket' => Ticket::where('created_at', '>=', $last_two_day)->count(),
        'open_ticket' => Ticket::where('status', 'close')->count(),
        'complete_ticket' => Ticket::count() != 0 ? (Ticket::where('status', 'close')->count() * 100) / Ticket::count() : 0
      ];
    }

    return response($data);
  }
}
