<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests\StoreCompany;
use App\Services\CompanyService;

use App\User;
use App\Models\Company\Company;
use App\Models\Company\CompanyPlan;
use App\Models\Company\CompanyUser;

use Illuminate\Session\Store;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CompaniesExport;


class CompanyController extends Controller
{

  public function __construct()
  {
    $this->companyItems = 5;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    //Pagination Items
    $items = request()->items ? request()->items:$this->companyItems;

    $companies = (new CompanyService($request))->getCompanies()->with('companyUser')->paginate($items);
    return response()->json($companies);
  }

  public function getAllCompanies(Request $request)
  {
    $companies = (new CompanyService($request))->getCompanies()->with('companyUser')->get();
    return response()->json($companies);
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreCompany $request)
  {
    $user = User::create([
      'title' => $request->owner_title,
      'first_name' => $request->owner_first_name,
      'last_name' => $request->owner_last_name,
      'email' => $request->owner_email,
      'phone' => $request->owner_phone,
      'vat' => $request->vat,
      'rea' => $request->rea,
      'password' => Hash::make($request->owner_password),
      'role' => 'admin',
      'timezone' => $request->time_zone,
      'language' => $request->language,
    ]);

    $company = new Company();
    $company->fill($request->all());
    $company->participant_bgimage = asset('images/participant_default_bg.jpg');
    $company->save();


    $companyUser = CompanyUser::create([
      'user_id' => $user->id,
      'company_id' => $company->id,
      'receive_communication' => 1
    ]);

    $companyPlan = CompanyPlan::create([
      'company_id' => $company->id,
      'plan_id' => $request->company_plan_id
    ]);
    return response()->json($company);
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $company = Company::query()
      ->with('companyUser')
      ->with('companyPlan')
      ->find($id);
    return response()->json($company);
  }

  public function getCompanyCommunicationReceiver($id)
  {
    $companyuser = CompanyUser::where('company_id', $id)->where('receive_communication', 1)->with('user')->first();
    return response()->json($companyuser);
  }

  public function getSingleCompany()
  {
    $id = Auth()->user()->companyUser->company_id;
    $company = Company::findOrFail($id);
    return response()->json($company);
  }

  public function updateSingleCompany(Request $request)
  {
    $company = Company::findOrFail($request->id);
    $company->fill($request->all());
    $company->save();
    return response()->json($company);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function update(StoreCompany $request, $id)
  {
      $data = [
        'title'     => $request->owner_title,
        'first_name'=> $request->owner_first_name,
        'last_name' => $request->owner_last_name,
        'email'     => $request->owner_email,
        'phone'     => $request->owner_phone,
        'password'  => Hash::make($request->owner_password),
        'address'   => $request->address,
        'timezone'  => $request->time_zone,
        'language'  => $request->language,
      ];

      User::query()->where('id', $request->owner_id)->update($data);

      $company_data = [
        'name'      => $request->name,
        'phone'     => $request->phone,
        'email'     => $request->email,
        'address'   => $request->address,
        'vat'  => $request->vat,
        'rea'  => $request->rea,
        'logo'      => $request->logo,
        'participant_bgimage' => asset('images/participant_default_bg.jpg')
      ];
      Company::query()->where('id', $id)->update($company_data);

      CompanyPlan::query()->updateOrCreate(
        ['company_id' => $id],
        ['plan_id'    => $request->company_plan_id]
      );

      return response()->json('Success!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      return $id;
  }

  //Export Pdf Company Data

  public function exportPdf(Request $request)
  {
    $data['companies'] = (new CompanyService($request))->getCompanies()->get();
    $fileName = "file_" . rand(000000, 999999) . ".pdf";
    $path = storage_path('app/public/export-pdf/') . $fileName;
    PDF::loadView('export-pdf.companylist', $data)->save($path);
    return response()->json(asset("storage/export-pdf/" . $fileName));
  }

  //Export Excel Company Data
  public function exportExcel(Request $request)
  {
  $file_name = rand(1000, 999999) . ".xlsx";
  Excel::store(new CompaniesExport($request), "export-excel/" . $file_name, 'public');
  return response()->json(asset("storage/export-excel/" . $file_name));
  }
}
