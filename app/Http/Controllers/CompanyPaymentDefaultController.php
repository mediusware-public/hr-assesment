<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests\StoreCompany;
use App\Services\CompanyService;

use App\User;
use App\Models\Company\Company;
use App\Models\Company\CompanyPlan;
use App\Models\Company\CompanyUser;
use App\Models\Company\CompanyPaymentDefault;
use Illuminate\Support\Facades\Notification;
use App\Notifications\SendPaymentInvoice;

use Illuminate\Session\Store;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CompaniesExport;


class CompanyPaymentDefaultController extends Controller
{

  public function __construct()
  {
    $this->companyItems = 5;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $companyPaymentDefault = CompanyPaymentDefault::where('company_id', Auth()->user()->companyUser->company_id)
      ->first();

    return response()->json($companyPaymentDefault);
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if($request->payment_method){
      $companyPaymentDefault = CompanyPaymentDefault::query()->updateOrCreate([
        'company_id' => Auth()->user()->companyUser->company_id
      ], [
        'payment_method' => $request->payment_method
      ]);

      return response()->json($companyPaymentDefault);
    } else {
      $companyPaymentDefault = CompanyPaymentDefault::query()
        ->where('company_id', Auth()->user()->companyUser->company_id)
        ->delete();

      return response()->json($companyPaymentDefault);
    }
  }


  public function sendInvoice(Request $request, $payment_id=null)
  {


    // return response()->json($invoice_data);
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function update(StoreCompany $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }


  //Export Pdf Company Data
  public function exportPdf(Request $request)
  {
    $data['companies'] = (new CompanyService($request))->getCompanies()->get();
    $fileName = "file_" . rand(000000, 999999) . ".pdf";
    $path = storage_path('app/public/export-pdf/') . $fileName;
    PDF::loadView('export-pdf.companylist', $data)->save($path);
    return response()->json(asset("storage/export-pdf/" . $fileName));
  }

  //Export Excel Company Data
  public function exportExcel(Request $request)
  {
    $file_name = rand(1000, 999999) . ".xlsx";
    Excel::store(new CompaniesExport($request), "export-excel/" . $file_name, 'public');
    return response()->json(asset("storage/export-excel/" . $file_name));
  }
}
