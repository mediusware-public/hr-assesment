<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests\StoreCompany;
use App\Services\CompanyService;

use App\User;
use App\Models\Company\Company;
use App\Models\Company\CompanyPlan;
use App\Models\Company\CompanyUser;
use App\Models\Company\CompanyPaymentHistory;
use Illuminate\Support\Facades\Notification;
use App\Notifications\SendPaymentInvoice;

use Illuminate\Session\Store;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CompaniesExport;


class CompanyPaymentHistoryController extends Controller
{

  public function __construct()
  {
    $this->companyItems = 5;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $today = Carbon::now()->toDateString();
    $paymentStatus = CompanyPaymentHistory::where('company_id', Auth()->user()->companyUser->company_id)
      ->where('date_to', '>=', $today)
      ->orderBy('date_to', 'desc')
      ->first();
    $lastPaid = CompanyPaymentHistory::where('company_id', Auth()->user()->companyUser->company_id)
      ->orderBy('date_to', 'desc')
      ->get()
      ->groupBy('payment_id')
      ->take(1)
      ->toArray();
    //$lastSum = ['total' => 0, 'months' => 0];
    /*foreach(current($lastPaid) as $last){
      dd($last);
      $lastSum['payment_id'] = $last->payment_id;
      $lastSum['total'] += $last->amount;
      $lastSum['months'] += 1;
    }*/
    //dd(count($lastPaid), current([]), count(current([])));

    return response()->json(['paymentStatus' => $paymentStatus, 'lastPaid' => $lastPaid]);
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

    $payments = $request->all();
    $company_id = $payments[0]['company_id'];

    $companyPaymentHistory = CompanyPaymentHistory::where('company_id', $company_id)->orderBy('date_to', 'desc')->first();

    $date = Carbon::now()->toDateString();

    if ($companyPaymentHistory) {
      foreach ($payments as $key => $payment) {
        if ($key == 0) $date = $companyPaymentHistory->date_to;
        else $date = Carbon::parse($date)->addMonth()->toDateString();
        $payments[$key]['user_id'] = Auth()->user()->id;
        $payments[$key]['date_from'] = $date;
        $payments[$key]['date_to'] = Carbon::parse($date)->addMonth()->toDateString();
        $payments[$key]['created_at'] = Carbon::now();
        $payments[$key]['updated_at'] = Carbon::now();
      }

    } else {
      foreach ($payments as $key => $payment) {
        if ($key == 0) $date = Carbon::now()->toDateString();
        else $date = Carbon::parse($date)->addMonth()->toDateString();
        $payments[$key]['user_id'] = Auth()->user()->id;
        $payments[$key]['date_from'] = $date;
        $payments[$key]['date_to'] = Carbon::parse($date)->addMonth()->toDateString();
        $payments[$key]['created_at'] = Carbon::now();
        $payments[$key]['updated_at'] = Carbon::now();
      }
    }

    CompanyPaymentHistory::insert($payments);

    return response()->json($payments);
  }


  public function sendInvoice(Request $request, $payment_id=null)
  {


    //for fake paypal purpose
    if(!empty($request->payment_method)){
      $companyPaymentHistory = CompanyPaymentHistory::where('payment_id', $request->payment_id)->get();
      foreach($companyPaymentHistory as $history){
        $history->payment_method = $request->payment_method;
      }
    }

    if(!empty($payment_id)){
      $companyPaymentHistory = CompanyPaymentHistory::where('payment_id', $payment_id)->get();
    }



    $user = User::find($companyPaymentHistory[0]->user_id)->with('companyUser')->first();

    $invoice_data = ['payment_data'=>$companyPaymentHistory, 'user'=>$user];
    Notification::route('mail', $user->email)
      ->notify(new SendPaymentInvoice($invoice_data));
    return response()->json($invoice_data);
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function update(StoreCompany $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }


  //Export Pdf Company Data
  public function exportPdf(Request $request)
  {
    $data['companies'] = (new CompanyService($request))->getCompanies()->get();
    $fileName = "file_" . rand(000000, 999999) . ".pdf";
    $path = storage_path('app/public/export-pdf/') . $fileName;
    PDF::loadView('export-pdf.companylist', $data)->save($path);
    return response()->json(asset("storage/export-pdf/" . $fileName));
  }

  //Export Excel Company Data
  public function exportExcel(Request $request)
  {
    $file_name = rand(1000, 999999) . ".xlsx";
    Excel::store(new CompaniesExport($request), "export-excel/" . $file_name, 'public');
    return response()->json(asset("storage/export-excel/" . $file_name));
  }
}
