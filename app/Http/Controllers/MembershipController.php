<?php
namespace App;
namespace App\Http\Controllers;

use App\Models\Company\Company;
use App\Models\Product\Product;
use App\Services\MembershipPlanService;
use Illuminate\Http\Request;
use App\Http\Requests\StorePlan;

use App\Models\Membership\Plan;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Carbon\Carbon;
use App\Exports\PlansExport;
use App\User;
use \Stripe\StripeClient;

class MembershipController extends Controller
{

    public function __construct()
    {
//      $this->stripeApiKey = 'sk_test_51HUtZNFTA0IDTfw7RhssQhxNflsZ2j0I6V4krYobtvoglyQYj23X3UoLZHWwLqLkCgEDKFPG1w4aWoyzAzUJu0pz00yv0cfc85';
      $this->stripeApiKey = 'sk_test_rbEmPFbvBXiBvRQsTEItUXHa00UwSbcUPn';
      $this->planItems = 5;
    }

    public function index(Request $request)
    {
      //Pagination Items
      $items = request()->items ? request()->items:$this->planItems;
      //get all tickets
      return response()->json((new MembershipPlanService($request))->getPlans()->paginate($items));
    }

    //Export Pdf Plan Data
    public function exportPdf(Request $request)
    {
      $data['plans'] = (new MembershipPlanService($request))->getPlans()->get();
      $fileName = "file_" . rand(000000, 999999) . ".pdf";
      $path = storage_path('app/public/export-pdf/').$fileName;
      PDF::loadView('export-pdf.planslist', $data)->save($path);
      return response()->json(asset("storage/export-pdf/".$fileName));
    }

    //Export Excel Plan Data
    public function exportExcel(Request $request)
    {
      $file_name = rand(1000, 999999).".xlsx";
      Excel::store(new PlansExport($request), "export-excel/".$file_name, 'public');
      return response()->json(asset("storage/export-excel/".$file_name));
    }

    public function store(StorePlan $request)
    {
      $plan = new Plan();
      $plan->fill($request->all());
      $plan->save();

      return response()->json($plan);
    }


    public function show($id)
    {
      $plan = Plan::find($id);
      return response()->json($plan);
    }


    public function update(Request $request, $id)
    {
      $plan = Plan::find($id);
      $plan->fill($request->all());
      $plan->save();
      return response()->json($plan);
    }

    public function destroy($id)
    {
      $plan = Plan::where('id', $id)->delete();
      return response()->json($plan);
    }

    public function newSubscribe(Request $request, $id)
    {
      $plan = Plan::find($request->plan);
      $amount = $plan->price * $plan->interval * 100;

      if($request->this_payment == 'stripe') {
        $stripe = new \Stripe\StripeClient(
          $this->stripeApiKey
        );

        $stripeConfirm = $stripe;
        $stripeRes = $stripe->paymentIntents->create([
          'amount' => $amount,
          'currency' => $plan->currency,
          'payment_method_types' => ['card'],
        ]);

        $stripe = $stripeConfirm;

        $stripeRes2 = $stripe->paymentIntents->confirm(
          $stripeRes->id,
          ['payment_method' => 'pm_card_visa']
        );
      }


      $customer = [];
      $user = [];
      if($request->saveThisCard) {
        $user = User::where('id', Auth()->user()->id);
        if($request->this_payment == 'stripe') {
          \Stripe\Stripe::setApiKey(
            $this->stripeApiKey
          );
          $customer = \Stripe\Customer::create([
              'source' => 'tok_mastercard',
              'email' => $user->first()->email
            ]
          );
          $user = $user->update(['stripe_id' => $customer->id]);
        }
        else if($request->this_payment == 'paypal') {
          $user = $user->update(['paypal_id' => $request->payer_id]);
        }
      } else {
        $user = User::where('id', Auth()->user()->id)->first();
      }

      if($request->this_payment == 'stripe' && $stripeRes2->amount == $stripeRes2->amount_received) {
        Company::where('id', request()->company_id)->where('subscribed_at', NULL)->update(['subscribed_at' => date('Y-m-d')]);

        return response()->json([
          'status' => 'new Success!',
          'intent' => $stripeRes,
          'conf' => $stripeRes2,
          'plan' => $plan,
          'customer' => $customer,
          'user' => $user
        ]);
      } else if($request->this_payment == 'paypal') {
        Company::where('id', request()->company_id)
          ->where('subscribed_at', NULL)
          ->update(['subscribed_at' => date('Y-m-d')]);

        return response()->json([
          'status' => 'new Success!',
          'user'   => $user,
          'params' => $request->all(),
          'plan'   => $plan
        ]);
      } else {
        return response()->json([
          'message' => 'Something went wrong, try again',
          'plan' => $plan,
          'customer' => $customer,
          'user' => $user
        ], 406);
      }
    }

    public function oldSubscriberPayment(Request $request, $id)
    {
      $plan = Plan::find($request->plan);
      $amount = $plan->price * $plan->interval * 100;

      $stripeRes = $stripeRes2 = $customer = $user = [];
      if($request->this_payment == 'stripe') {
        if ($request->continueSaved) {
          \Stripe\Stripe::setApiKey(
            $this->stripeApiKey
          );
          $stripeRes2 = \Stripe\Charge::create([
            'amount' => $amount,
            'currency' => $plan->currency,
            'customer' => $request->customer,
          ]);
        } else {
          $stripe = new \Stripe\StripeClient(
            $this->stripeApiKey
          );

          $stripeConfirm = $stripe;
          $stripeRes = $stripe->paymentIntents->create([
            'amount' => $amount,
            'currency' => $plan->currency,
            'payment_method_types' => ['card'],
          ]);

          $stripe = $stripeConfirm;

          $stripeRes2 = $stripe->paymentIntents->confirm(
            $stripeRes->id,
            ['payment_method' => 'pm_card_visa']
          );

          if ($request->saveThisCard) {
            \Stripe\Stripe::setApiKey(
              $this->stripeApiKey
            );
            $user = User::where('id', Auth()->user()->id);
            $customer = \Stripe\Customer::create([
                'source' => 'tok_mastercard',
                'email' => $user->first()->email
              ]
            );
            $user = $user->update(['stripe_id' => $customer->id]);
          }
        }
      } elseif($request->this_payment == 'paypal') {

      }

      if($request->this_payment == 'stripe' && ($stripeRes2->amount == $stripeRes2->amount_received || $stripeRes2->amount == $stripeRes2->amount_captured)) {
        Company::where('id', request()->company_id)
          ->where('subscribed_at', NULL)
          ->update(['subscribed_at' => date('Y-m-d')]);

        return response()->json([
          'status' => 'old Success!',
          'intent' => $stripeRes,
          'conf' => $stripeRes2,
          'plan' => $plan,
          'customer' => $customer,
          'user' => $user
        ]);
      } elseif($request->this_payment == 'paypal') {
        Company::where('id', request()->company_id)
          ->where('subscribed_at', NULL)
          ->update(['subscribed_at' => date('Y-m-d')]);

        return response()->json([
          'status' => 'old Success!',
          'intent' => $stripeRes,
          'conf' => $stripeRes2,
          'plan' => $plan,
          'customer' => $customer,
          'user' => $user
        ]);
      } else {
        return response()->json(['message' => 'Something went wrong, try again', 'response' => $stripeRes2], 406);
      }
    }

    public function newSubscribeOld(Request $request, $id)
    {
      $user          = $id ? User::find($id) : Auth()->user();
      $priceId       = $request->plan;
      $paymentMethod = $request->payment_method;

      $user->newSubscription('default', $priceId)->create($paymentMethod);

      return response()->json(['status' => 'Success!']);
    }
}
