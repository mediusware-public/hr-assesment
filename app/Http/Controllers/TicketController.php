<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\StoreTicket;
use App\Exports\TicketsExport;

use App\Services\TicketService;

use App\Models\Tickets\Ticket;
use App\Models\Tickets\TicketFile;
use Illuminate\Support\Arr;
use PDF;
use Maatwebsite\Excel\Facades\Excel;

class TicketController extends Controller
{

    public function __construct()
    {
        $this->ticketItems = 10;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Pagination Items
        $items = request()->items ? request()->items:$this->ticketItems;

        //get all tickets
        return response()->json((new TicketService($request))->getTickets()
          ->where('parent_id', NULL)
          ->orderBy('id', 'desc')
          ->paginate($items));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTicket $request)
    {

      $data = $request->all();
      $data['user_id'] = request()->user_id ? request()->user_id:Auth()->user()->id;
      $data['project_id'] = $request->project_id ? $request->project_id:NULL;

      $ticket = Ticket::create($data);
      if($request->my_files) {
        foreach ($request->my_files as $key => $file) {
          if($request->my_files[$key]!=null) {
            $files[$key] = [
              'ticket_id' => $ticket->id,
              'file' => $file,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now(),
            ];
          }
        }
        $ticketFile = TicketFile::insert($files);
      }
      return response()->json($ticket);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return response()->json(Ticket::query()
        ->where('parent_id', NULL)
        ->where('id', $id)
        ->with('user:id,name')
        ->with('files:id,ticket_id,file')
        // ->with('replies')
        ->first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTicket $request, $id)
    {
      $ticket = Ticket::where('id', $id)->update($request->except(['my_files']));
      TicketFile::where('ticket_id', $id)->delete();

      if($request->my_files) {
        /*$files = [
          'ticket_id' => $id,
          'file' => $request->my_files
        ];*/
        foreach ($request->my_files as $key => $file) {

            $files[$key] = [
              'ticket_id' => $id,
              'file' => $file,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now(),
            ];
        }
        $ticketFile = TicketFile::insert($files);
      }
      return response()->json($ticket);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ticket = Ticket::where('id', $id)->delete();
        return response()->json($ticket);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteFile($id)
    {
        $ticketFile = TicketFile::where('id', $id)->delete();
        return response()->json($ticketFile);
    }

  //Export Pdf Plan Data
  public function exportPdf(Request $request)
  {
    $data['tickets'] = (new TicketService($request))->getTickets()->where('parent_id', NULL)->orderBy('id', 'desc')->get();
    $fileName = "file_" . rand(000000, 999999) . ".pdf";
    $path = storage_path('app/public/export-pdf/').$fileName;
    PDF::loadView('export-pdf.ticketslist', $data)->save($path);
    return response()->json(asset("storage/export-pdf/".$fileName));
  }

  //Export Excel Plan Data
  public function exportExcel(Request $request)
  {
    $file_name = rand(1000, 999999).".xlsx";
    Excel::store(new TicketsExport($request), "export-excel/".$file_name, 'public');
    return response()->json(asset("storage/export-excel/".$file_name));
  }

  // Get Super Admin
  public function getAllSuperAdmin()
  {
    $superAdmin = User::where('role', 'super_admin')->orderBy('first_name', 'ASC')->get();
    return response()->json($superAdmin);
  }
}
