<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\StoreTicketReplay;

use App\Services\TicketService;

use App\Models\Tickets\Ticket;
use App\Models\Tickets\TicketFile;

class TicketReplayController extends Controller
{

    public function __construct()
    {
        $this->ticketItems = 10;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTicketReplay $request)
    {
        $ticket = Ticket::create($request->except(['my_files']));
        if($request->my_files) {
            /*$files = [
                'ticket_id' => $ticket->id,
                'file' => $request->my_files
            ];*/
            foreach ($request->my_files as $key => $file) {
              if($request->my_files[$key]!=null){
                $files[$key] = [
                  'ticket_id' => $ticket->id,
                  'file'      => $file,
                  'created_at' => Carbon::now(),
                  'updated_at' => Carbon::now(),
                ];
            }
            }
            $ticketFile = TicketFile::insert($files);
        }
        $tickets = $this->getReplies($request->parent_id);
        return response()->json($tickets);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //get ticket all replies
        $ticket = Ticket::query()->with('files:id,ticket_id,file')->find($id);
        $replies = $this->getReplies($id);
        return response()->json(['replies' => $replies, 'ticket' => $ticket]);
    }

    //Fetching Replies
    public function getReplies($id)
    {
        //get all replies for ticket
        return Ticket::where('parent_id', $id)->with('user:id,title,first_name,last_name')->with('files')->orderBy('id', 'desc')->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTicketReplay $request, $id)
    {
        $ticket = Ticket::where('id', $id)->update($request->except(['my_files']));

        $files = [
            'ticket_id' => $id,
            'file'      => $request->my_files
        ];
        /*foreach ($request->my_files as $key => $file) {
            $files[$key] = [
                'ticket_id' => 1,
                'file'      => $file,
                ];
        }*/
        $ticketFile = TicketFile::create($files);

        return response()->json($ticket);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ticket = Ticket::where('id', $id)->delete();
        return response()->json($ticket);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteFile($id)
    {
        $ticketFile = TicketFile::where('id', $id)->delete();
        return response()->json($ticketFile);
    }
}
