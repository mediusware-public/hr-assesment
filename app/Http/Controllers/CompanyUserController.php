<?php

namespace App\Http\Controllers;

use App\Exports\CompanyAllParticipantExport;
use App\Exports\CompanyAllUsersExport;
use App\Helpers\HoganHelper;
use App\Models\Company\ParticipantProject;
use App\Models\Hogan\Hogan;
use App\Models\Passport\OAuthAccessToken;
use Carbon\Carbon;
use App\Exports\CompanyUserExport;
use Illuminate\Http\Request;


use App\Http\Requests\StoreCompanyUser;
use App\Services\CompanyUserService;

use App\User;
use App\Models\Company\Company;
use App\Models\Company\CompanyPlan;
use App\Models\Company\CompanyUser;
use App\Models\Company\Project;

use Illuminate\Session\Store;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CompaniesExport;


class CompanyUserController extends Controller
{

  public function __construct()
  {
    $this->companyItems = 10;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    //Pagination Items
    $items = request()->items ? request()->items : $this->companyItems;

    $companies = (new CompanyUserService($request))->getCompanyUsers()->paginate($items);
    return response()->json($companies);
  }

  public function CompanyAllUsersList($request, $id) {
    $users = User::query()
      ->whereIn('role', ['admin', 'hr'])->with('companyUser')->whereHas('companyUser', function ($query) use($request, $id){
        $query->where('company_id', $id);
        //Searches

        if ($request->name) $query->where(function ($query) use($request) {
          $query->where('first_name', 'like', '%' . $request->name . '%')
            ->orWhere('last_name', 'like', '%' . $request->name . '%');
        });
        if ($request->phone) $query->where('phone', 'like', '%' . $request->phone . '%');
        if ($request->email) $query->where('email', 'like', '%' . $request->email . '%');
        if ($request->address) $query->where('address', 'like', '%' . $request->address . '%');
      });

    return $users;
  }


  public function getCompanyUsersById(Request $request, $id)
  {
    //Pagination Items
    $items = request()->items ? request()->items : $this->companyItems;
    return $this->CompanyAllUsersList($request, $id)->paginate($items);
  }

  //Export Company All Participant Data
  public function exportCompanyAllUsersPdf(Request $request, $id)
  {
    $data['companyusers'] = $this->CompanyAllUsersList($request, $id)->get();
    $fileName = "file_" . rand(000000, 999999) . ".pdf";
    $path = storage_path('app/public/export-pdf/') . $fileName;
    PDF::loadView('export-pdf.companyuserslist', $data)->save($path);
    return response()->json(asset("storage/export-pdf/" . $fileName));
  }

  //Export Excel Company All Participant
  public function exportCompanyAllUsersExcel(Request $request, $id)
  {
    $file_name = rand(1000, 999999) . ".xlsx";
    Excel::store(new CompanyAllUsersExport($request, $id), "export-excel/" . $file_name, 'public');
    return response()->json(asset("storage/export-excel/" . $file_name));
  }




  public function CompanyAllParticipantList($request, $id) {
    $participants = User::query()
    ->where('role', 'participant')->whereHas('companyUser', function ($query) use($request, $id){
      $query->where('company_id', $id);

      //Searches
      if ($request->name) $query->where(function ($query) use($request) {
        $query->where('first_name', 'like', '%' . $request->name . '%')
          ->orWhere('last_name', 'like', '%' . $request->name . '%');
      });
      if ($request->phone) $query->where('phone', 'like', '%' . $request->phone . '%');
      if ($request->email) $query->where('email', 'like', '%' . $request->email . '%');
      if ($request->address) $query->where('address', 'like', '%' . $request->address . '%');
    });

    return $participants;
  }

  public function getCompanyParticipantsById(Request $request, $id)
  {
    //Pagination Items
    $items = request()->items ? request()->items : $this->companyItems;
    return $this->CompanyAllParticipantList($request, $id)->paginate($items);
  }

  //Export Company All Participant Data
  public function exportCompanyAllParticipantPdf(Request $request, $id)
  {
    $data['companyusers'] = $this->CompanyAllParticipantList($request, $id)->get();
    $fileName = "file_" . rand(000000, 999999) . ".pdf";
    $path = storage_path('app/public/export-pdf/') . $fileName;
    PDF::loadView('export-pdf.companyuserslist', $data)->save($path);
    return response()->json(asset("storage/export-pdf/" . $fileName));
  }

  //Export Excel Company All Participant
  public function exportCompanyAllParticipantExcel(Request $request, $id)
  {
    $file_name = rand(1000, 999999) . ".xlsx";
    Excel::store(new CompanyAllParticipantExport($request, $id), "export-excel/" . $file_name, 'public');
    return response()->json(asset("storage/export-excel/" . $file_name));
  }




  public function getCompanyByUserId(Request $request, $id)
  {
    $userCompany = User::query()
      ->where('id', $id)
      ->with('companyUser')
      ->first();

    return $userCompany;
  }


  public function getCompanyUserLogById($id)
  {

    //return User::with('oAuthAccessTokens')->find(1);
    $query = CompanyUser::with('user', 'company', 'usersWithLogHistory')->where('id', $id)->get();


    return $query->map(function ($history) {
      if (!empty($history->usersWithLogHistory->oAuthAccessTokens->id)) {
        return [
          'id' => !empty($history->usersWithLogHistory->oAuthAccessTokens->id) ? $history->usersWithLogHistory->oAuthAccessTokens->id : "",
          'company_name' => $history->company->name,
          'user_name' => $history->user->name,
          'phone_no' => !empty($history->user->phone) ? $history->user->phone : '',
          'role' => $history->role,
          'device_name' => !empty($history->usersWithLogHistory->oAuthAccessTokens->name) ? $history->usersWithLogHistory->oAuthAccessTokens->name : "",
          'login_time' => !empty($history->usersWithLogHistory->oAuthAccessTokens->created_at) ? $history->usersWithLogHistory->oAuthAccessTokens->created_at : "",
        ];
      } else {
        return [];
      }
    });


    //return OAuthAccessToken::all();
  }


  public function getProjectParticipantList(Request $request, $slug)
  {
    $project = Project::where('company_id', auth()->user()->companyUser->company_id)->where('slug', $slug)->first();

    if ($request->search_value) {
      $user = User::where('role', 'participant')->whereHas('companyUser', function ($query) use ($project) {
        $query->where('company_id', auth()->user()->companyUser->company_id)->where('project_id', $project->id);
      })->where('email', 'like', '%' . $request->search_value . '%')->take(20)->pluck('email');
    } else {
      $user = User::where('role', 'participant')->whereHas('companyUser', function ($query) use ($project) {
        $query->where('company_id', auth()->user()->companyUser->company_id)->where('project_id', $project->id);
      })->take(20)->pluck('email');
    }
    return response()->json($user);
  }

  //Export Project Participant Data
  public function exportPdf(Request $request)
  {
    $data['companyusers'] = (new CompanyUserService($request))->getCompanyUsers()->get();
    $fileName = "file_" . rand(000000, 999999) . ".pdf";
    $path = storage_path('app/public/export-pdf/') . $fileName;
    $pdf = PDF::loadView('export-pdf.companyuserslist', $data)->save($path);
    return response()->json(asset("storage/export-pdf/" . $fileName));
  }

  //Export Excel Project Participant
  public function exportExcel(Request $request)
  {
    $file_name = rand(1000, 999999) . ".xlsx";
    Excel::store(new CompanyUserExport($request), "export-excel/" . $file_name, 'public');
    return response()->json(asset("storage/export-excel/" . $file_name));
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreCompanyUser $request)
  {
    $exist_user = User::where('email', $request->email)->first();
    if (!$exist_user) {
      $tokenInfo = HoganHelper::getAccessToken();
      if (array_key_exists("error", $tokenInfo->responseData)) {
        return response()->json(['message' => $tokenInfo->responseData['error_description']], 400);
      } else {
        try {
          $project = Project::where('company_id', auth()->user()->companyUser->company_id)->where('slug', $request->project_slug)->first();
          $participantId = null;
          $password = $request->password ? $request->password : 'password';

          $responseData = Http::withToken($tokenInfo->responseData['access_token'])->post($tokenInfo->url . '/Clients/' . $tokenInfo->halo_clientid . '/Groups/' . $project->name . '/Participants', [
            'clientUserId' => $tokenInfo->user_id,
            'groupName' => $project->name,
            'numberOfParticipants' => 1,
            'password' => $password,
            'setGroupOptions' => 'true',
          ]);

          $participantData = $responseData->json();
          $participantId = $participantData['participants'][0]['participantId'];

          //Update Participant Profile
          $participantProfile = Http::withToken($tokenInfo->responseData['access_token'])->post($tokenInfo->url . '/Clients/' . $tokenInfo->halo_clientid . '/Groups/' . $project->name . '/Participants/' . $participantId, [
            'clientUserId' => $tokenInfo->user_id,
            'groupName' => $project->name,
            'participantId' => $participantId,
            'title' => $request->title,
            'firstName' => $request->first_name,
            'lastName' => $request->last_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address
          ]);

          $user = User::create([
            'title' => $request->title,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($password),
            'phone' => $request->phone,
            'address' => $request->address,
            'timezone' => $request->time_zone,
            'role' => 'participant',
            'participantId' => $participantId,
            'participantPassword' => $password,
          ]);

          $companyUser = CompanyUser::create([
            'user_id' => $user->id,
            'company_id' => auth()->user()->companyUser->company_id,
            'project_id' => $project->id,
            'receive_communication' => 0,
          ]);

          $participantProject = ParticipantProject::create([
            'participant_id' => $user->id,
            'project_id' => $project->id,
          ]);

          return response()->json($participantProfile->json());
        } catch (\Exception $e) {
          return response()->json(['message' => $e->getMessage()], 400);
        }
      }
    } else {
      return response()->json(['message' => 'Project Already Exists.'], 400);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $companyUser = User::query()->find($id);
    return response()->json($companyUser);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function update(StoreCompanyUser $request, $id)
  {
    $user = [
      'title' => $request->title,
      'first_name' => $request->first_name,
      'last_name' => $request->last_name,
      'email' => $request->email,
      'phone' => $request->phone,
      'address' => $request->address,
      'timezone' => $request->time_zone
    ];
    User::query()->where('id', $id)->update($user);
    return response()->json('Update Success!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    return $id;
  }

  public function participantNoteUpdate(Request $request, $id)
  {
    $user = User::findOrFail($id);
    $user->participant_note = $request->participant_note;
    $user->update();
    return response()->json('Update Success!');
  }
}
