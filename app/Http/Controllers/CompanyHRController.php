<?php

namespace App\Http\Controllers;

use App\Helpers\HoganHelper;
use App\Models\Company\ParticipantProject;
use App\Models\Hogan\Hogan;
use App\Models\Passport\OAuthAccessToken;
use Carbon\Carbon;
use App\Exports\CompanyUserExport;
use Illuminate\Http\Request;


use App\Http\Requests\StoreCompanyUser;
use App\Services\CompanyUserService;

use App\User;
use App\Models\Company\Company;
use App\Models\Company\CompanyPlan;
use App\Models\Company\CompanyUser;
use App\Models\Company\Project;

use Illuminate\Session\Store;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CompaniesExport;


class CompanyHRController extends Controller
{

  public function __construct()
  {
    $this->companyItems = 10;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    //Pagination Items
    $items = request()->items ? request()->items : $this->companyItems;

    $companyUser = CompanyUser::query()
      ->with('user')
      ->where('company_id',  auth()->user()->companyUser->company_id)
      ->whereHas('user', function ($query) use ($request) {
        $query->whereIn('role',['hr', 'admin']);

        //Searches
        if ($request->name) $query->where('name', 'like', '%' . $request->name . '%');
        if ($request->phone) $query->where('phone', 'like', '%' . $request->phone . '%');
        if ($request->email) $query->where('email', 'like', '%' . $request->email . '%');
        if ($request->address) $query->where('address', 'like', '%' . $request->address . '%');
      })
      ->paginate($items);

    return $companyUser;
  }


  public function getCompanyUsersById(Request $request, $id)
  {
    //Pagination Items
    $items = request()->items ? request()->items : $this->companyItems;

    $companyUser = CompanyUser::query()
      ->with('user')
      ->where('company_id', $id)
      ->whereHas('user', function ($query) use ($request) {
        $query->whereIn('role', ['admin', 'hr']);

        //Searches
        if ($request->name) $query->where('name', 'like', '%' . $request->name . '%');
        if ($request->phone) $query->where('phone', 'like', '%' . $request->phone . '%');
        if ($request->email) $query->where('email', 'like', '%' . $request->email . '%');
        if ($request->address) $query->where('address', 'like', '%' . $request->address . '%');
      })
      ->paginate($items);

    return $companyUser;
  }

  public function getCompanyHRsById(Request $request, $id)
  {
    //Pagination Items
    $items = request()->items ? request()->items : $this->companyItems;

    $companyUser = CompanyUser::query()
      ->with('user')
      ->where('company_id', $id)
      ->whereHas('user', function ($query) use ($request) {
        $query->where('role', 'hr');

        //Searches
        if ($request->name) $query->where('name', 'like', '%' . $request->name . '%');
        if ($request->phone) $query->where('phone', 'like', '%' . $request->phone . '%');
        if ($request->email) $query->where('email', 'like', '%' . $request->email . '%');
        if ($request->address) $query->where('address', 'like', '%' . $request->address . '%');
      })
      ->paginate($items);

    return $companyUser;
  }

  public function changeAccessorCommunication(Request $request) {
    $user = User::where('id', $request->id)->with('companyUser')->first();
    CompanyUser::where('company_id', $user->companyUser->company_id)->update(['receive_communication'=> 0]);
    $companyuser = CompanyUser::where('user_id', $request->id)->update(['receive_communication'=> 1]);
    return response()->json($companyuser);
  }







  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreCompanyUser $request)
  {
    $exist_user = User::where('email', $request->email)->first();
    if (!$exist_user) {

      try {




        $user = User::create([
          'title' => $request->title,
          'first_name' => $request->first_name,
          'last_name' => $request->last_name,
          'name' => $request->name,
          'email' => $request->email,
          'password' => Hash::make($request->password),
          'phone' => $request->phone,
          'address' => $request->address,
          'timezone' => $request->time_zone,
          'role' => $request->role,

        ]);

        $companyUser = CompanyUser::create([
          'user_id' => $user->id,
          'company_id' => $request->companyId?$request->companyId:auth()->user()->companyUser->company_id,
          'project_id' => null,
          'receive_communication' => 0,
        ]);

        return response()->json($user);
      } catch (\Exception $e) {
        return response()->json(['message' => $e->getMessage()], 400);
      }
    } else {
      return response()->json(['message' => 'Project Already Exists.'], 400);
    }
  }

  public function update(StoreCompanyUser $request, $id)
  {
    $user = [
      'first_name' => $request->first_name,
      'last_name' => $request->last_name,
      'email' => $request->email,
      'phone' => $request->phone,
      'address' => $request->address,
      'timezone' => $request->time_zone,
      'role' => $request->role,
    ];
    User::query()->where('id', $id)->update($user);
    return response()->json('Update Success!');
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $companyUser = User::query()->find($id);
    return response()->json($companyUser);
  }


  public function destroy($id)
  {
    return $id;
  }
}
