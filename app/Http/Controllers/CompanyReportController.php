<?php

namespace App\Http\Controllers;

use App\Models\Company\CompanyUser;
use App\Models\Company\CompanyReport;
use App\Models\Company\Report;

use Illuminate\Http\Request;
use App\Http\Requests\StoreTicket;
use App\Exports\TicketsExport;
use Illuminate\Support\Facades\Http;

use App\Services\TicketService;

use App\Models\Tickets\Ticket;
use App\Models\Tickets\TicketFile;
use App\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use PDF;
use Maatwebsite\Excel\Facades\Excel;

class CompanyReportController extends Controller
{

    public function __construct()
    {
      $this->reportItems = 1;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Pagination Items
        $items = request()->items ? request()->items:$this->reportItems;

        //get all projects
        return response()->json(CompanyReport::paginate(100));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = $request->except(['company_id', 'project_id']);
      CompanyReport::where('company_id', request()->company_id)->where('project_id', request()->project_id)->delete();
      $companyReport = CompanyReport::insert($data);
      return $companyReport;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getProjectReports($company_id, $project_id)
    {
//      return [$company_id, $project_id];
      $myReport = [];
      $companyReport = CompanyReport::where('company_id', $company_id)->where('project_id', $project_id)->with('report:id,key,value')->get();
      foreach($companyReport as $report) {
        $myReport[] = $report->report;
      }
      return response()->json($myReport);
    }

    public function getAllReports()
    {
      $reports = Report::query()->get(['id', 'key', 'value']);
      return response()->json($reports);
    }

    public function show($slug)
    {
      $company_user = CompanyUser::where('user_id', auth()->user()->id)->first();
      $project = CompanyReport::query()->where('slug', $slug)->where('company_id', $company_user->company_id)->first();
      return response()->json($project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteFile($id)
    {

    }
}
