<?php

namespace App\Http\Controllers;

use App\Models\Language;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Exports\TicketsExport;

use App\Services\TicketService;

use Illuminate\Support\Arr;
use PDF;
use Maatwebsite\Excel\Facades\Excel;

class LanguageController extends Controller
{

    public function __construct()
    {
        $this->items = 10;
        $this->defaultLanguage = 'en';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $languages = Language::orderBy('code', 'asc')->where('code', '!=', $this->defaultLanguage)->get();
      return response()->json($languages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $defaultLanguage = $request->defaultLanguage;
      $customLanguage  = $request->customLanguage;

      Language::where('code', $defaultLanguage['code'])
        ->update([
          'language' => json_encode($defaultLanguage['language'])
        ]);
      Language::where('code', $customLanguage['code'])
        ->update([
          'language' => json_encode($customLanguage['language'])
        ]);

      return response()->json([$defaultLanguage, $customLanguage]);
    }

    /**
     * Display the specified resource.
     *
     * @param  mixed  $code
     * @return \Illuminate\Http\Response
     */
    public function show($code)
    {
      $languageData = Language::where('code', $code)->firstOrCreate([
        'code' => $code
      ],[
        'language' => "[]"
      ]);
      return response()->json($languageData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $language = Language::where('id', $id);
      $language->update(['status' => !$language->first()->status]);
      return response()->json($language->first());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteFile($id)
    {
      //
    }

  //Export Pdf Plan Data
  public function exportPdf(Request $request)
  {
    $data['tickets'] = (new TicketService($request))->getTickets()->where('parent_id', NULL)->orderBy('id', 'desc')->get();
    $fileName = "file_" . rand(000000, 999999) . ".pdf";
    $path = storage_path('app/public/export-pdf/').$fileName;
    PDF::loadView('export-pdf.ticketslist', $data)->save($path);
    return response()->json(asset("storage/export-pdf/".$fileName));
  }

  //Export Excel Plan Data
  public function exportExcel(Request $request)
  {
    $file_name = rand(1000, 999999).".xlsx";
    Excel::store(new TicketsExport($request), "export-excel/".$file_name, 'public');
    return response()->json(asset("storage/export-excel/".$file_name));
  }

  // Get Super Admin
  public function getAllSuperAdmin()
  {
    //
  }
}
