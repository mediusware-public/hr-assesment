<?php

namespace App\Http\Controllers;

use App\Imports\ParticipantsImport;
use App\Models\Mail\Mail;
use App\Notifications\CredentialSendMail;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Http\Requests\StoreUser;
use Illuminate\Support\Facades\Http;
use App\Services\UserService;
use Illuminate\Support\Facades\Notification;
use App\User;
use Illuminate\Support\Str;

use Laravel\Cashier\Cashier;
use Laravel\Cashier\PaymentMethod;
use DB;

use \Stripe\Plan;

class UserController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function __construct()
  {
    $this->items = 5;
  }

  public function index(Request $request)
  {
    //Pagination Items
    $items = request()->items ? request()->items : $this->items;
    $users = (new UserService($request))->getOnlyUsers()->paginate($items);
    return response()->json($users);
  }

  public function roleWiseUsers()
  {
    $role = Auth()->user()->role;
    if ($role == 'super_admin') {
      $users = User::orderBy('name', 'asc')->where('role', 'admin')->get();
    }
    if ($role == 'admin') {
      $users = User::orderBy('name', 'asc')->whereIn('role', ['hr', 'participant'])->get();
    }
    return response()->json($users);
  }

  public function getAdminUsers(Request $request)
  {
    //Pagination Items
    $items = request()->items ? request()->items : $this->items;
    $users = (new UserService($request))->getOnlyAdmin();
    if(request()->items == 0) $users = $users->select('id', DB::raw("CONCAT(`title`,' ',`first_name`,' ',`last_name`) AS full_name"))->get();
    else $users = $users->paginate($items);;
    return response()->json($users);
  }

  public function getHrUsers(Request $request)
  {
    //Pagination Items
    $items = request()->items ? request()->items : $this->items;
    $users = (new UserService($request))->getOnlyHr()->paginate($items);
    return response()->json($users);
  }

  public function getSuperAdminUsers(Request $request)
  {
    //Pagination Items
    $items = request()->items ? request()->items : $this->items;
    $users = (new UserService($request))->getOnlySuperAdmin()->paginate($items);
    return response()->json($users);
  }

  public function import(Request $request)
  {
    $file = $request->file('file')->store('public/import-excel');
    $import = new ParticipantsImport($request);
    $import->import($file);
    if ($import->failures()->isNotEmpty()) {
      return response()->json($import->failures(), 400);
    } else {
      return response()->json('Successfully File Imported.');
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreUser $request)
  {
    //
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $user = User::find($id);
    return response()->json($user);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }


  public function getIntent(Request $request, $id)
  {
    $user = User::find($id);
    $intent = $user->createSetupIntent();
    return response()->json($intent);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }

  public function sendCredentials(Request $request)
  {
    try {
      $ids = $request->ids;
      if (!empty($ids)) {
        foreach ($ids as $id) {
          $user = User::find($id);
          $user->remember_token = Str::uuid();
          $user->update();
          $mail = new Mail();
          $mail->mail_to = $user->email;
          $mail->mail_from = auth()->user()->companyUser->company->email;
          $mail->mail_subject = "Credential Send";
          $mail->mail_body = "Credential Send";
          $mail->mail_attachment = [];
          $mail->user_id = auth()->id();
          $mail->save();
          //Sending mail to email
          Notification::route('mail', $user->email)
            ->notify(new CredentialSendMail($user)); //With data
        }
        return response()->json(['status' => 'success', 'message' => 'Credential send Successfully']);
      }
      return response()->json(['status' => 'error', 'message' => 'Credential send Fail']);
    } catch (\Exception $e) {
      return response()->json(['status' => 'error', 'message' => $e->getMessage()]);
    }
  }
}
