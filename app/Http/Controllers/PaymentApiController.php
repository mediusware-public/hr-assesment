<?php
namespace App;
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StorePaymentAPI;

use App\Models\Settings\Payment;

class PaymentApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get Payment
        $payment = Payment::first();
        if($payment) {
            return response()->json($payment);
        } else {
            $paymentsApiData = (object) [
                "paypal_environment" => "",
                "paypal_customer_id" => "",
                "paypal_customer_secret" => "",
                "stripe_environment" => "",
                "stripe_publisher_key" => "",
                "stripe_secret_key" => "",
                "stripe_webhook_signing_secret_key" => "",
                "has_paypal" => false,
                "has_stripe" => false
            ];
            return response()->json($paymentsApiData);
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePaymentAPI $request)
    {
        $payment = Payment::firstOrNew();
        $payment->fill($request->all());
        $payment->save();
        return response()->json($payment);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
