<?php

namespace App\Imports;

use App\Helpers\HoganHelper;
use App\Models\Company\CompanyUser;
use App\Models\Company\ParticipantProject;
use App\Models\Company\Project;
use App\User;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Validators\Failure;
use Throwable;
use Illuminate\Support\Facades\Http;


class ParticipantsImport implements ToCollection, WithHeadingRow, SkipsOnError, WithValidation, SkipsOnFailure, WithChunkReading
{
  use Importable, SkipsErrors, SkipsFailures;

  public function __construct(Request $request)
  {
    $this->request = $request;
  }

  public function collection(Collection $rows)
  {
    $emails = [];
    foreach ($rows as $key => $row) {
      $emails[] = $row['email'];
    }
    //Get Existing Participant Email
    $existing_users_mails = User::whereIn('email', $emails)->pluck('email')->toArray();
    $uniqueRow = count($rows) - count($existing_users_mails);

    if(count($rows) != count($existing_users_mails)) {
      $tokenInfo = HoganHelper::getAccessToken();
      if(array_key_exists("error",$tokenInfo->responseData))
      {
        return response()->json(['message' => $tokenInfo->responseData['error_description']], 400);
      } else {
        try {
          $project = Project::where('company_id', auth()->user()->companyUser->company_id)->where('slug', $this->request->project_slug)->first();
          $participantId = null;
          $password = 'password';

          $responseData = Http::withToken($tokenInfo->responseData['access_token'])->post($tokenInfo->url.'/Clients/' . $tokenInfo->halo_clientid . '/Groups/' . $project->name . '/Participants', [
            'clientUserId' => $tokenInfo->user_id,
            'groupName' => $project->name,
            'numberOfParticipants' => $uniqueRow,
            'password' => $password,
            'setGroupOptions' => 'true',
          ]);

          $responseParticipantJsonData = $responseData->json();
          $participantData = $responseParticipantJsonData['participants'];


           foreach ($rows as $key => $row) {
//            if(!in_array($row['email'], $existing_users_mails)) {
              //Update Participant Profile
              $participantProfile = Http::withToken($tokenInfo->responseData['access_token'])->post($tokenInfo->url.'/Clients/' . $tokenInfo->halo_clientid . '/Groups/' . $project->name . '/Participants/' . $participantData[$key]['participantId'], [
                'clientUserId' => $tokenInfo->user_id,
                'groupName' => $project->name,
                'participantId' => $participantData[$key]['participantId'],
                'title' => $row['title'],
                'firstName' => $row['firstname'],
                'lastName' => $row['lastname'],
                'email' => $row['email'],
                'phone' => $row['phone'],
                'address' => $row['address']
              ]);

              //Create Participant
              $user = User::create([
                'title' => $row['title'],
                'first_name' => $row['firstname'],
                'last_name' => $row['lastname'],
                'email' => $row['email'],
                'password' => Hash::make($password),
                'phone' => $row['phone'],
                'address' => $row['address'],
                'role' => 'participant',
                'participantId' => $participantData[$key]['participantId'],
                'participantPassword' => $password,
              ]);

              $companyUser = CompanyUser::create([
                'user_id' => $user->id,
                'company_id' => auth()->user()->companyUser->company_id,
                'project_id' => $project->id,
                'receive_communication' => 0,
              ]);

              $participantProject = ParticipantProject::create([
                'participant_id' => $user->id,
                'project_id' => $project->id,
              ]);
            }
//          }
        } catch (\Exception $e) {
          return response()->json(['message' => $e->getMessage() ], 400);
        }
      }
    }
  }



    public function rules(): array
    {
      return [
        '*.email' => ['email', 'unique:users,email']
      ];
    }

//    public function onFailure(Failure ...$failures)
//    {
//    }


    public function onError(Throwable $e)
    {

    }

  public function chunkSize(): int
  {
    return 100;
  }

}
