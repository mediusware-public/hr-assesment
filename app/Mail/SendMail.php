<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
  use Queueable, SerializesModels;

  protected $mail_data;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct($mail_data)
  {
    $this->mail_data = $mail_data;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    $email = $this->markdown('emails.sendmail')
      ->with("mail_data", $this->mail_data)
      ->from('admin@galileo.com', 'Galileo-Admin')
      ->subject($this->mail_data->mail_subject);

    // $attachments is an array with file paths of attachments
    if (!empty($this->mail_data->mail_attachment)) {
      foreach ($this->mail_data->mail_attachment as $filePath) {
        $email->attach($filePath);
      }
    }

    return $email;

//        return $this->markdown('emails.sendmail');
  }
}
