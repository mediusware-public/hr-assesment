<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordReset extends Mailable
{
  use Queueable, SerializesModels;

  protected $mail_data;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct($mail_data)
  {
    $this->mail_data = $mail_data;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    $email = $this->markdown('emails.password-reset')
      ->with("mail_data", $this->mail_data)
      ->from('admin@galileo.com', 'Galileo-Admin')
      ->subject("Password reset");

    return $email;

//        return $this->markdown('emails.password-reset');
  }
}
