<?php

namespace App\Jobs;

use App\Mail\SendMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendEmailJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  protected $mail_data;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct($mail_data)
  {
    $this->mail_data = $mail_data;
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {

    $email = new SendMail($this->mail_data);
    Mail::to($this->mail_data->mail_to)->send($email);
  }
}
