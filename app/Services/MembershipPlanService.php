<?php
namespace App\Services;

use App\Models\Membership\Plan;

class MembershipPlanService
{
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function getPlans()
    {

        $plans = Plan::query();

        if($this->request->name)        $plans->where('name', 'like', '%'.$this->request->name.'%');
        if($this->request->description) $plans->where('description', 'like', '%'.$this->request->description.'%');

        return $plans;
    }

}
