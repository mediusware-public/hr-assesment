<?php

namespace App\Services;

use App\Models\Tickets\Ticket;

class TicketService
{
  private $request;
  private $filter_by_user;

  public function __construct($request)
  {
    $this->request = $request;
    $this->filter_by_user = $this->getTickets();
  }

  public function getTickets()
  {
    $tickets = Ticket::query();

    $role = Auth()->user()->role;

    if ($this->request->user_id && $this->request->user_id != 'all') $tickets->where('user_id', $this->request->user_id);
    if ($this->request->priority && $this->request->priority != 'all') $tickets->where('priority', $this->request->priority);
    if ($this->request->status && $this->request->status != 'all') $tickets->where('status', $this->request->status);
    if ($this->request->title) $tickets->where(function ($query) {
      $query->where('title', 'like', "%{$this->request->title}%")->orWhere('body', 'like', "%{$this->request->title}%");
    });

    $tickets->with('user:id,title,first_name,last_name,role');


    if ($role == 'super_admin') $tickets->whereHas('user', function ($query) {
      return $query->where('role', '=', 'admin');
    });

    if ($role == 'admin') {
      if ($this->request->project_id) $tickets->where('project_id', $this->request->project_id);
      /*$tickets->whereHas('user', function ($query) {
        return $query->whereIn('role', ['hr', 'participant']);
      });*/

    }

    $tickets->with('files:id,ticket_id,file')->with('replies');

    return $tickets;
  }

  public function getTicketByUser()
  {
    return $this->filter_by_user->where('user_id', auth()->user()->id);
  }
}
