<?php

namespace App\Services;

use App\Models\File\Media;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CloudFilePicker
{
  private $request;

  public function __construct($request)
  {
    $this->request = $request;
  }

  public function getDropBoxFile()
  {
    $uploader_id = auth()->user()->id;
    $company_id = isset(auth()->user()->companyUser->company_id) ? auth()->user()->companyUser->company_id : null;

    foreach ($this->request->files_data as $file_r) {
      $param = $file_r;
      $fileName = $param['name'];
      $fileUrl = $param['link'];
      $file = file_get_contents($fileUrl);
      $filename = pathinfo($fileUrl, PATHINFO_FILENAME); //file name without extension
      $extension = pathinfo($fileUrl, PATHINFO_EXTENSION); // to get extension
      $file_info = new \finfo(FILEINFO_MIME_TYPE);
      $filetype = $file_info->buffer(file_get_contents($fileUrl));
      $fileNameToStore = Str::uuid() . '_' . time() . '.' . $extension;
//    $path = $file->storeAs('upload', $fileNameToStore, 'public');
      $file_url = "storage/upload/" . $fileNameToStore;
      $fileUpload = new Media();
      $fileUpload->alt = '';
      $fileUpload->title = $fileName;
      $fileUpload->path = $file_url;
      $fileUpload->type = $filetype;
      $fileUpload->caption = '';
      $fileUpload->description = '';
      $fileUpload->slug = Str::slug($fileName, '-');
      $fileUpload->user_id = $uploader_id;
      $fileUpload->modified_by = $uploader_id;
      $fileUpload->company_id = $company_id;
      $fileUpload->save();
      Storage::disk('local')->put('public/upload/' . $fileNameToStore, $file);
    }
  }

  public function getGoogleDriveFile()
  {
    $param = $this->request;
    $uploader_id = auth()->user()->id;
    $company_id = isset(auth()->user()->companyUser->company_id) ? auth()->user()->companyUser->company_id : null;

    $oAuthToken = $param['oAuthToken'];
    $fileId = $param['fileId'];
    $fileName = $param['name'];
    $fileUrl = $param['url'];
    $getUrl = 'https://www.googleapis.com/drive/v2/files/' . $fileId . '?alt=media';
    $authHeader = 'Authorization: Bearer ' . $oAuthToken;

    // $response = Http::withHeaders(array($authHeader))->get($getUrl);
    // dd($response->throw()->json());

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $getUrl);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array($authHeader));
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    $data = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);
    $file = $data;

    $arr = explode('.', $fileName);
    $extension = end($arr);
    $filename = $fileName;
    $filetype = $this->get_mime_type($extension);
    $fileNameToStore = Str::uuid() . '_' . time() . '.' . $extension;
//    $path = $file->storeAs('upload', $fileNameToStore, 'public');
    $file_url = "storage/upload/" . $fileNameToStore;
    $fileUpload = new Media();
    $fileUpload->alt = '';
    $fileUpload->title = $fileName;
    $fileUpload->path = $file_url;
    $fileUpload->type = $filetype;
    $fileUpload->caption = '';
    $fileUpload->description = '';
    $fileUpload->slug = Str::slug($fileName, '-');
    $fileUpload->user_id = $uploader_id;
    $fileUpload->modified_by = $uploader_id;
    $fileUpload->company_id = $company_id;
    $fileUpload->save();
    Storage::disk('local')->put('public/upload/' . $fileNameToStore, $file);
    return $fileUpload;
  }

  //One drive File
  public function getOneDriveFile()
  {
    $uploader_id = auth()->user()->id;
    $company_id = isset(auth()->user()->companyUser->company_id) ? auth()->user()->companyUser->company_id : null;

    foreach ($this->request->files_data as $file_r) {
      $param = $file_r;
      $fileName = $param['name'];
      $fileUrl = $param['@content.downloadUrl'];
      $ch = curl_init($fileUrl);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);

      $data = curl_exec($ch);
      $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      $content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
      $error = curl_errno($ch);
      curl_close($ch);


      $fileExtension = explode('.', $fileName);


      $file = file_get_contents($fileUrl);
//      $filename = pathinfo($fileUrl, PATHINFO_FILENAME);
//      $extension = pathinfo($fileUrl, PATHINFO_EXTENSION);
      $file_info = new \finfo(FILEINFO_MIME_TYPE);
      $filetype = $file_info->buffer($file);
      $fileNameToStore = Str::uuid() . '_' . time() . '.' . $fileExtension[1];
      $file_url = "storage/upload/" . $fileNameToStore;
      file_put_contents($file_url, $data);
      $fileUpload = new Media();
      $fileUpload->alt = '';
      $fileUpload->title = $fileName;
      $fileUpload->path = $file_url;
      $fileUpload->type = $filetype;
      $fileUpload->caption = '';
      $fileUpload->description = '';
      $fileUpload->slug = Str::slug($fileName, '-');
      $fileUpload->user_id = $uploader_id;
      $fileUpload->modified_by = $uploader_id;
      $fileUpload->company_id = $company_id;
      $fileUpload->save();
      Storage::disk('local')->put('public/upload/' . $fileNameToStore, $file);
    }
  }

  private function get_mime_type($extension)
  {

    $mimet = array(
      'txt' => 'text/plain',
      'htm' => 'text/html',
      'html' => 'text/html',
      'php' => 'text/html',
      'css' => 'text/css',
      'js' => 'application/javascript',
      'json' => 'application/json',
      'xml' => 'application/xml',
      'swf' => 'application/x-shockwave-flash',
      'flv' => 'video/x-flv',

      // images
      'png' => 'image/png',
      'jpe' => 'image/jpeg',
      'jpeg' => 'image/jpeg',
      'jpg' => 'image/jpeg',
      'gif' => 'image/gif',
      'bmp' => 'image/bmp',
      'ico' => 'image/vnd.microsoft.icon',
      'tiff' => 'image/tiff',
      'tif' => 'image/tiff',
      'svg' => 'image/svg+xml',
      'svgz' => 'image/svg+xml',

      // archives
      'zip' => 'application/zip',
      'rar' => 'application/x-rar-compressed',
      'exe' => 'application/x-msdownload',
      'msi' => 'application/x-msdownload',
      'cab' => 'application/vnd.ms-cab-compressed',

      // audio/video
      'mp3' => 'audio/mpeg',
      'qt' => 'video/quicktime',
      'mov' => 'video/quicktime',

      // adobe
      'pdf' => 'application/pdf',
      'psd' => 'image/vnd.adobe.photoshop',
      'ai' => 'application/postscript',
      'eps' => 'application/postscript',
      'ps' => 'application/postscript',

      // ms office
      'doc' => 'application/msword',
      'rtf' => 'application/rtf',
      'xls' => 'application/vnd.ms-excel',
      'ppt' => 'application/vnd.ms-powerpoint',
      'docx' => 'application/msword',
      'xlsx' => 'application/vnd.ms-excel',
      'pptx' => 'application/vnd.ms-powerpoint',


      // open office
      'odt' => 'application/vnd.oasis.opendocument.text',
      'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
    );

    if (isset($mimet[$extension])) {
      return $mimet[$extension];
    } else {
      return 'application/octet-stream';
    }
  }
}
