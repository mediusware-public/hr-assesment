<?php

namespace App\Services;

use App\Models\Company\Project;
use App\User;

class CompanyUserService
{
  private $request;
  private $filter_by_user;

  public function __construct($request)
  {
    $this->request = $request;
    $this->filter_by_user = $this->getCompanyUsers();
    $this->filter_for_company_id = $this->getCompanyUsers();
  }

  public function getCompanyUsers()
  {
    $project = Project::where('company_id', auth()->user()->companyUser->company_id)->where('slug', $this->request->project_slug)->first();
    $user = User::query();
    $user->where('role', 'participant')->whereHas('companyUser', function ($query) use($project){
      $query->where('company_id', auth()->user()->companyUser->company_id)->where('project_id', $project->id);
    })->with('participantAssessments');

    if ($this->request->name) $user->where('first_name', 'like', '%' . $this->request->name . '%');
    if ($this->request->phone) $user->where('phone', 'like', '%' . $this->request->phone . '%');
    if ($this->request->email) $user->where('email', 'like', '%' . $this->request->email . '%');
    if ($this->request->address) $user->where('address', 'like', '%' . $this->request->address . '%');

    return $user;
  }

  public function getCompanyByUser()
  {
    return $this->filter_by_user->where('user_id', auth()->user()->id);
  }

  public function getCompanyUsersById()
  {
    return $this->filter_for_company_id->where('user_id', auth()->user()->id);
  }
}
