<?php
namespace App\Services;

use App\Models\Company\Company;

class CompanyService
{
    private $request;
    private $filter_by_user;

    public function __construct($request)
    {
        $this->request        = $request;
        $this->filter_by_user = $this->getCompanies();
    }

    public function getCompanies()
    {
        $companies = Company::query();

        if($this->request->name)    $companies->where('name', 'like', '%'.$this->request->name.'%');
        if($this->request->phone)   $companies->where('phone', 'like', '%'.$this->request->phone.'%');
        if($this->request->email)   $companies->where('email', 'like', '%'.$this->request->email.'%');
        if($this->request->address) $companies->where('address', 'like', '%'.$this->request->address.'%');

        return $companies;
    }

    public function getCompanyByUser()
    {
        return $this->filter_by_user->where('user_id', auth()->user()->id);
    }
}