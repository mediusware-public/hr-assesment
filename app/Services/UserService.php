<?php
namespace App\Services;

use App\User;

class UserService
{
    private $request;
    private $filter_by_user;

    public function __construct($request)
    {
        $this->request        = $request;
        $this->filter_by_user = $this->getCompanyUsers();
    }

    public function getCompanyUsers()
    {
        $users = User::query();

        if ($this->request->name) $users->where(function ($users)  {
        $users->where('first_name', 'like', '%' . $this->request->name . '%')
            ->orWhere('last_name', 'like', '%' . $this->request->name . '%');
        });

//        if($this->request->name)    $users->where('name', 'like', '%'.$this->request->name.'%');
        if($this->request->phone)   $users->where('phone', 'like', '%'.$this->request->phone.'%');
        if($this->request->email)   $users->where('email', 'like', '%'.$this->request->email.'%');
        if($this->request->address) $users->where('address', 'like', '%'.$this->request->address.'%');

        return $users;
    }

  public function getOnlyUsers()
  {
    return $this->filter_by_user->where('role', 'participant');
  }

  public function getOnlyHr()
  {
    return $this->filter_by_user->where('role', 'hr');
  }

  public function getOnlyAdmin()
  {
    return $this->filter_by_user->where('role', 'admin');
  }

  public function getOnlySuperAdmin()
  {
    return $this->filter_by_user->where('role', 'super_admin');
  }

}
