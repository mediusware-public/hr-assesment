<?php

namespace App\Exports;
use App\Services\MembershipPlanService;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Http\Request;

class PlansExport implements ShouldAutoSize, WithMapping, WithHeadings, WithEvents, FromCollection
{

    public function __construct(Request $request)
    {
      $this->request = $request;
    }

    public function collection()
    {
        return (new MembershipPlanService($this->request))->getPlans()->get();
    }

    public function map($plan): array
    {
        return [
            $plan['name'],
            $plan['price'],
            $plan['currency'],
            $plan['interval'],
            $plan['description'],
        ];
    }

    public function headings(): array
    {
        return [
            'Name',
            'Price',
            'Currency',
            'Interval',
            'Description',
        ];
    }

  public function registerEvents(): array
  {
    return [
      AfterSheet::class => function (AfterSheet  $event) {
        $event->sheet->getStyle('A1:E1')->applyFromArray([
          'font' => [
            'bold' => true
          ]
        ]);
      }
    ];
  }

}
