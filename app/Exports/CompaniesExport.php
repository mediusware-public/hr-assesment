<?php

namespace App\Exports;

use App\Models\Company\Company;
use App\Services\CompanyService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;

class CompaniesExport implements ShouldAutoSize, WithMapping, WithHeadings, WithEvents, FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function __construct(Request $request)
    {
      $this->request = $request;
    }

    public function collection()
    {
      return (new CompanyService($this->request))->getCompanies()->with('companyUser')->with('companyPlan')->get();
    }


    public function map($company): array
    {
        return [
            $company['name'],
            $company['email'],
            $company['phone'],
            $company['address'],
        ];
    }

    public function headings(): array
    {
        return [
            'Name',
            'Email',
            'Phone',
            'Address'
        ];
    }

    public function registerEvents(): array
  {
    return [
      AfterSheet::class => function (AfterSheet  $event) {
        $event->sheet->getStyle('A1:D1')->applyFromArray([
          'font' => [
            'bold' => true
          ]
        ]);
      }
    ];
  }
}
