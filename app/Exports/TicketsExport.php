<?php

namespace App\Exports;

use App\Services\TicketService;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Http\Request;

class TicketsExport implements ShouldAutoSize, WithMapping, WithHeadings, WithEvents, FromCollection
{
  public function __construct(Request $request)
  {
    $this->request = $request;
  }

  public function collection()
  {
    return (new TicketService($this->request))->getTickets()->where('parent_id', NULL)->orderBy('id', 'desc')->get();
  }

  public function map($plan): array
  {
    return [
      $plan['title'],
      $plan['body'],
      $plan['status'],
      $plan['user']['name'],
    ];
  }

  public function headings(): array
  {
    return [
      'Title',
      'Description',
      'Status',
      'Created By',
    ];
  }

  public function registerEvents(): array
  {
    return [
      AfterSheet::class => function (AfterSheet  $event) {
        $event->sheet->getStyle('A1:D1')->applyFromArray([
          'font' => [
            'bold' => true
          ]
        ]);
      }
    ];
  }
}
