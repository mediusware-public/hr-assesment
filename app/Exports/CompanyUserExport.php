<?php

namespace App\Exports;

use App\Services\CompanyUserService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;

class CompanyUserExport implements ShouldAutoSize, WithMapping, WithHeadings, WithEvents, FromCollection
{
  /**
   * @return \Illuminate\Support\Collection
   */
  public function __construct(Request $request)
  {
    $this->request = $request;
  }

  public function collection()
  {
    return (new CompanyUserService($this->request))->getCompanyUsers()->get();
  }


  public function map($companyuser): array
  {
    return [
      $companyuser['name'],
      $companyuser['email'],
      $companyuser['phone'],
      $companyuser['address'],
    ];
  }

  public function headings(): array
  {
    return [
      'Name',
      'Email',
      'Phone',
      'Address'
    ];
  }

  public function registerEvents(): array
  {
    return [
      AfterSheet::class => function (AfterSheet  $event) {
        $event->sheet->getStyle('A1:D1')->applyFromArray([
          'font' => [
            'bold' => true
          ]
        ]);
      }
    ];
  }
}
