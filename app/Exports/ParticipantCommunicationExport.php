<?php

namespace App\Exports;

use App\Services\CompanyUserService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Models\Mail\Mail;
use App\User;

class ParticipantCommunicationExport implements ShouldAutoSize, WithMapping, WithHeadings, WithEvents, FromCollection
{
  /**
   * @return \Illuminate\Support\Collection
   */
  public function __construct(Request $request)
  {
    $this->request = $request;
  }

  public function collection()
  {
    $participant = User::find($this->request->participant_id);
    return Mail::where('mail_from', auth()->user()->email)->where('mail_to', $participant->email)->with('user')->get();
  }


  public function map($participant_communication): array
  {
    return [
      $participant_communication['created_at'],
      $participant_communication['mail_subject'],
      $participant_communication['mail_body'],
    ];
  }

  public function headings(): array
  {
    return [
      'Date',
      'Subject',
      'Body'
    ];
  }

  public function registerEvents(): array
  {
    return [
      AfterSheet::class => function (AfterSheet  $event) {
        $event->sheet->getStyle('A1:C1')->applyFromArray([
          'font' => [
            'bold' => true
          ]
        ]);
      }
    ];
  }
}
