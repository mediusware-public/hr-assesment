<?php

namespace App\Exports;

use App\Services\CompanyUserService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Models\Mail\Mail;
use App\Models\Company\Project;

class CompanyProjectCommunicationExport implements ShouldAutoSize, WithMapping, WithHeadings, WithEvents, FromCollection
{
  /**
   * @return \Illuminate\Support\Collection
   */
  public function __construct(Request $request)
  {
    $this->request = $request;
  }

  public function collection()
  {
    $project = Project::query()->where('slug', $this->request->project_slug)->where('company_id', auth()->user()->companyUser->company_id)->first();
    //Sent Mails
    return Mail::where('company_id', auth()->user()->companyUser->company_id)->where('project_id', $project->id)->with('user')->get();
  }


  public function map($participant_communication): array
  {
    return [
      $participant_communication['created_at'],
      $participant_communication['mail_subject'],
      $participant_communication['mail_body'],
    ];
  }

  public function headings(): array
  {
    return [
      'Date',
      'Subject',
      'Body'
    ];
  }

  public function registerEvents(): array
  {
    return [
      AfterSheet::class => function (AfterSheet  $event) {
        $event->sheet->getStyle('A1:C1')->applyFromArray([
          'font' => [
            'bold' => true
          ]
        ]);
      }
    ];
  }
}
