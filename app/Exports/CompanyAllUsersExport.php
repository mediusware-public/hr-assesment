<?php

namespace App\Exports;

use App\Services\CompanyUserService;
use App\Http\Controllers\CompanyUserController;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;

class CompanyAllUsersExport implements ShouldAutoSize, WithMapping, WithHeadings, WithEvents, FromCollection
{
  /**
   * @return \Illuminate\Support\Collection
   */
  public function __construct(Request $request, $id)
  {
    $this->request = $request;
    $this->id = $id;
  }

  public function collection()
  {
    return (new CompanyUserController())->CompanyAllUsersList($this->request, $this->id)->get();
  }

  public function map($companyuser): array
  {
    return [
      $companyuser['name'],
      $companyuser['email'],
      $companyuser['phone'],
      $companyuser['address'],
    ];
  }

  public function headings(): array
  {
    return [
      'Name',
      'Email',
      'Phone',
      'Address'
    ];
  }

  public function registerEvents(): array
  {
    return [
      AfterSheet::class => function (AfterSheet  $event) {
        $event->sheet->getStyle('A1:D1')->applyFromArray([
          'font' => [
            'bold' => true
          ]
        ]);
      }
    ];
  }
}
