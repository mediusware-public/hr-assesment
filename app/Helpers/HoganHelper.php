<?php
namespace App\Helpers;
use App\Models\Hogan\Hogan;

use Illuminate\Support\Facades\Http;

class HoganHelper
{
  public static function getAccessToken()
  {
    $hogan_credentials = Hogan::where('company_id', auth()->user()->companyUser->company_id)->first();
    $url = $hogan_credentials->active == 0 ? $hogan_credentials->development_link : $hogan_credentials->production_link;
    $user_id = $hogan_credentials->active == 0 ? $hogan_credentials->development_user_id : $hogan_credentials->production_user_id;
    $hogan_password = $hogan_credentials->active == 0 ? $hogan_credentials->development_password : $hogan_credentials->production_password;
    $master_clientid = $hogan_credentials->active == 0 ? $hogan_credentials->development_client_id : $hogan_credentials->production_client_id;
    $halo_clientid = $hogan_credentials->active == 0 ? $hogan_credentials->development_halo_client_id : $hogan_credentials->production_halo_client_id;

    try {
      $response = Http::asForm()->post($url . '/token', [
        'grant_type' => 'password',
        'username' => $user_id,
        'password' => $hogan_password,
        'clientid' => $master_clientid,
      ]);

      return (object)[
        'responseData' => $response->json(),
        'user_id' => $user_id,
        'halo_clientid' => $halo_clientid,
        'url' => $url,
      ];
    } catch (\Exception $e) {
      return response()->json(['message' => $e->getMessage() ], 400);
    }
  }
}
