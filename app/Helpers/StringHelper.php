<?php
namespace App\Helpers;

class StringHelper
{
  public static function makeId($val, $length)
  {
    $id = '';
    if(strlen($val) >= $length) $id = $val;
    else {
      for($i = 0; $i < $length - strlen($val); $i++){
        $id .= '0';
      }
      $id .= $val;
    }
    return $id;
  }
}
