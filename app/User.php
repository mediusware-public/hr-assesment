<?php

namespace App;

use App\Models\Company\Company;
use App\Models\Company\CompanyUser;
use App\Models\Company\ParticipantAssessment;
use App\Models\Passport\OAuthAccessToken;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
  use Notifiable, HasApiTokens, Billable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'first_name',
    'last_name',
    'name',
    'email',
    'password',
    'photo',
    'timezone',
    'address','participant_note',
    'role',
    'phone',
    'status',
    'title',
    'paypal_id',
    'fatture_id',
    'participantId','participantPassword',
    'language'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password',
    'remember_token',
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'email_verified_at' => 'datetime',
  ];

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function companyUser()
  {
    return $this->hasOne(CompanyUser::class)->with('company');
  }

  public function getCreatedAtAttribute($value)
  {
    return Carbon::createFromTimeStamp(strtotime($value))->diffForHumans();
  }

  public function oAuthAccessTokens()
  {
    return $this->hasMany(OAuthAccessToken::class);
  }

  public function participantAssessments()
  {
    return $this->hasMany(ParticipantAssessment::class, 'participant_id');
  }

  /*public function getRoleAttribute($value)
  {
    return ucwords(str_replace("_", " ", $value));
  }*/
}
