<?php

namespace App\Notifications;

use PDF;
use App\Helpers\StringHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SendPaymentInvoice extends Notification implements ShouldQueue
{
  use Queueable;

  /**
   * Create a new notification instance.
   *
   * @return void
   */
  public $invoice_data;

  public function __construct($invoice_data)
  {
    $this->invoice_data = $invoice_data;
  }

  /**
   * Get the notification's delivery channels.
   *
   * @param mixed $notifiable
   * @return array
   */
  public function via($notifiable)
  {
    return ['mail'];
  }

  /**
   * Get the mail representation of the notification.
   *
   * @param mixed $notifiable
   * @return \Illuminate\Notifications\Messages\MailMessage
   */


  public function toMail($notifiable)
  {
    $data['invoice_data'] = $this->invoice_data;
    $invoice_data = $this->invoice_data;

    $fileName = "payment-invoice_" . rand(000000, 999999) . ".pdf";

    Storage::disk('local')->put('public/export-pdf/'.$fileName, 'public');

    $path = storage_path('app/public/export-pdf/') . $fileName;

    PDF::loadView('pdf.payment-invoice', $data)->save($path);

    return (new MailMessage)
      ->from('admin@galileo.com', 'Galileo-Admin')
      ->subject('Galileo - Payment Invoice #' . StringHelper::makeId($invoice_data['payment_data'][0]->id, 15))
      ->attach($path, [
        'as' => $fileName,
        'mime' => 'application/pdf',
      ])
      ->markdown('mail.payment-invoice', ['invoice_data' => $invoice_data]);
  }

  /**
   * Get the array representation of the notification.
   *
   * @param mixed $notifiable
   * @return array
   */
  public function toArray($notifiable)
  {
    return [
      //
    ];
  }
}
