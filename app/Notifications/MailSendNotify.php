<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class MailSendNotify extends Notification implements ShouldQueue
{
  use Queueable;

  /**
   * Create a new notification instance.
   *
   * @return void
   */
  public $mail_data;

  public function __construct($mail_data)
  {
    $this->mail_data = $mail_data;
  }

  /**
   * Get the notification's delivery channels.
   *
   * @param mixed $notifiable
   * @return array
   */
  public function via($notifiable)
  {
    return ['mail'];
  }

  /**
   * Get the mail representation of the notification.
   *
   * @param mixed $notifiable
   * @return \Illuminate\Notifications\Messages\MailMessage
   */
  public function toMail($notifiable)
  {
//    $file11 = $this->mail_data->mail_attachment[0];


      if ($this->mail_data->mail_attachment) {
        foreach($this->mail_data->mail_attachment as $file) {
          (new MailMessage)->attach($file, [
            'as' => rand(0000, 1111),
            'mime' => 'application/pdf'
          ]);
        }
      }
    return (new MailMessage)
      ->from('admin@galileo.com', 'Galileo-Admin')
      ->subject($this->mail_data->mail_subject)
      ->markdown('mail.send-mail', ['mail_data' => $this->mail_data]);
//    $file11 = $this->mail_data->mail_attachment[0];
//    ->attach($file11, [
//    'as' => 'filename111.pdf',
//    'mime' => 'application/pdf',
//  ])

//      if ($this->mail_data->mail_attachment) {
//        foreach($this->mail_data->mail_attachment as $file) {
//          (new MailMessage)->attach($file, [
//            'as' => 'filename.pdf',
//            'mime' => '*'
//          ]);
//        }
//      }
  }

  /**
   * Get the array representation of the notification.
   *
   * @param mixed $notifiable
   * @return array
   */
  public function toArray($notifiable)
  {
    return [
      //
    ];
  }
}
