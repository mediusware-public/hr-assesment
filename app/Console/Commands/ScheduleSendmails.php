<?php
namespace App\Console\Commands;
use App\Jobs\SendEmailJob;
use Illuminate\Console\Command;
use App\Models\Mail\Mail;
use Carbon\Carbon;

class ScheduleSendmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:sendmails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an email to users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      //One hour is added to compensate for PHP being one hour faster
      $now = date("Y-m-d H:i", strtotime(Carbon::now()->addHour(6)));
      $now = $now.":00";

      $mails = Mail::get();

      if($mails !== null){
        $mails->where('schedule_time',  $now)->each(function($mail) {
          if($mail->mail_sent == 0)
          {
            dispatch(new SendEmailJob($mail));
//            Notification::route('mail', $mail->mail_to)->notify(new MailSendNotify($mail));
            $mail->mail_sent = 1;
            $mail->save();
          }
        });
      }
    }
}
