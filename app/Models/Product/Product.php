<?php

namespace App\Models\Product;

use App\Models\Membership\Plan;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $fillable = [
    'name',
    'code',
    'type',
    'active'
  ];

  public function plan() {
    return $this->hasMany(Plan::class);
  }
}
