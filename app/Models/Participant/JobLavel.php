<?php

namespace App\Models\Participant;

use Illuminate\Database\Eloquent\Model;

class JobLavel extends Model
{
  protected $guarded = ['id'];
  protected $fillable = [
    'name',
    'active',
    'user_id'
  ];
}
