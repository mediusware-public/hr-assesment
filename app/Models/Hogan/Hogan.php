<?php

namespace App\Models\Hogan;

use Illuminate\Database\Eloquent\Model;

class Hogan extends Model
{
  protected $fillable = [
    'active',
    'production_link',
    'production_halo_client_id',
    'production_client_id',
    'production_user_id',
    'production_password',
    'development_link',
    'development_halo_client_id',
    'development_client_id',
    'development_user_id',
    'development_password',
    'company_id',
    'created_by'
  ];

  public function scopeMyCompany($query)
  {
    return $query->where('company_id', auth()->user()->companyUser->company_id);
  }
}
