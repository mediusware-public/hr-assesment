<?php
namespace App;
namespace App\Models\Mail;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
	public function user()
  {
    return $this->belongsTo('App\User');
	}

	protected  $casts = [
    'mail_attachment' => 'array',
  ];

	protected $fillable = [
		'mail_to',
		'mail_from',
		'mail_subject',
		'mail_attachment',
    'mail_sent',
    'schedule_time',
		'mail_starred',
		'user_id',
    'project_id',
    'company_id'
	];

}
