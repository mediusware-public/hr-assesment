<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assessment extends Model
{
  use SoftDeletes;

  protected $fillable = [
    'key',
    'value'
  ];
}
