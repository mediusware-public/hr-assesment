<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyAssessment extends Model
{
//  use SoftDeletes;

  protected $fillable = [
    'company_id',
    'project_id',
    'assessment_id'
  ];


  public function assessment()
  {
    return $this->belongsTo(Assessment::class);
  }
}
