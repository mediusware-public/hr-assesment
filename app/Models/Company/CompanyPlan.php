<?php

namespace App\Models\Company;
use App\Models\Membership\Plan;
use Illuminate\Database\Eloquent\Model;

class CompanyPlan extends Model
{
  protected $fillable = [
    'company_id',
    'plan_id'
  ];

  public function plan()
  {
    return $this->belongsTo(Plan::class, 'plan_id');
  }
}
