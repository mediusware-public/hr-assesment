<?php

namespace App\Models\Company;

use App\User;
use App\Models\Company\Company;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyPaymentHistory extends Model
{
  use SoftDeletes;
  protected $fillable = [
    'company_id',
    'user_id',
    'payment_type',
    'payment_id',

    'card_holder_name',

    'card_number',
    'card_expiration_date',
    'card_cvc',
    'card_zip',

    'currency',
    'amount',
    'date_from',
    'date_to',
    'payment_method',
    'status'
  ];
}
