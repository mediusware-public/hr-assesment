<?php

namespace App\Models\Company;

use App\User;
use App\Models\Company\Company;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyPaymentDefault extends Model
{
  use SoftDeletes;
  protected $fillable = [
    'company_id',
    'payment_method'
  ];
}
