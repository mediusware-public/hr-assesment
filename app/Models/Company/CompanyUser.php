<?php

namespace App\Models\Company;

use App\Models\Passport\OAuthAccessToken;
use App\User;
use Illuminate\Database\Eloquent\Model;

class CompanyUser extends Model
{
  protected $fillable = [
    'user_id',
    'company_id',
    'project_id',
    'receive_communication'
  ];


  public function user()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function userAsAdmin()
  {
    return $this->belongsTo(User::class, 'user_id')->where('role', 'admin');
  }

  public function company()
  {
    return $this->belongsTo(Company::class, 'company_id');
  }

  public function usersWithLogHistory()
  {
//    return $this->belongsTo(User::class,'user_id')->with('oAuthAccessTokens');
    return $this->hasMany(OAuthAccessToken::class, 'user_id');
  }

}
