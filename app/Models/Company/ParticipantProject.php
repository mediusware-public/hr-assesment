<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;

class ParticipantProject extends Model
{
  protected $fillable = [
    'participant_id',
    'project_id',
  ];
}
