<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyReport extends Model
{
//  use SoftDeletes;

  protected $fillable = [
    'company_id',
    'project_id',
    'report_id'
  ];


  public function report()
  {
    return $this->belongsTo(Report::class);
  }
}
