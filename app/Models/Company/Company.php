<?php

namespace App\Models\Company;

use App\Models\Membership\Plan;
use App\User;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Company extends Model
{
  protected $fillable = [
    'name',
    'email',
    'phone',
    'address',
    'logo',
    'city',
    'vat',
    'rea',
    'country',
    'user_id',
    'note',
    'participant_bgimage',
    'subscribed_at'
  ];

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function companyPlan()
  {
    return $this->hasOne(CompanyPlan::class)->with('plan');
  }

  public function companyUser()
  {
    return $this->hasOne(CompanyUser::class)->with('userAsAdmin');
  }

  public function companyUsers()
  {
    return $this->hasMany(CompanyUser::class)->with('user');
  }

  public function getCreatedAtAttribute($value)
  {
    return Carbon::createFromTimeStamp(strtotime($value))->diffForHumans();
  }

  public function companyPaymentHistory()
  {
    return $this->hasMany(CompanyPaymentHistory::class);
  }
}
