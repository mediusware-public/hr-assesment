<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class ParticipantAssessment extends Model
{
//  use SoftDeletes;

  protected $fillable = [
    'participant_id',
    'assessment_id'
  ];


  public function participant()
  {
    return $this->belongsTo(User::class);
  }

  public function assessment()
  {
    return $this->belongsTo(Assessment::class);
  }
}
