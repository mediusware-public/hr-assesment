<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
  protected $guarded = ['id'];
  protected $appends = ['file_url'];

  public function getFileUrlAttribute()
  {
    return asset($this->path);
  }

  public function scopeOwnFiles($query)
  {
    return $query->where('user_id', auth()->user()->id);
  }
}
