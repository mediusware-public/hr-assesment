<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'paypal_environment',
        'paypal_customer_id',
        'paypal_customer_secret',
        'stripe_environment',
        'stripe_publisher_key',
        'stripe_secret_key',
        'stripe_webhook_signing_secret_key',
    ];
}
