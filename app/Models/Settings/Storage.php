<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class Storage extends Model
{
    protected $fillable = [
        'google_drive_developer_key',
        'google_drive_client_id',
        'google_drive_app_id',
        'google_drive_scopes',
        'dropbox_client_id',
        'dropbox_client_secret',
        'onedrive_client_id',
        'onedrive_client_secret',
        's3_client_id',
        's3_client_secret',
        'fatture_api_client_id',
        'fatture_api_client_secret',
        'updated_by'
    ];
}
