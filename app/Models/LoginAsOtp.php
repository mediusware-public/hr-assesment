<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoginAsOtp extends Model
{
  // use SoftDeletes;

  /*protected $date = [
      'deleted_at'
  ];*/

	protected $fillable = [
    'email',
		'token'
	];
}