<?php

namespace App\Models\Tickets;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketFile extends Model
{
    use SoftDeletes;

    protected $date = [
        'deleted_at'
    ];

    protected $fillable = [
        'ticket_id',
        'file'
    ];
}
