<?php

namespace App\Models\Tickets;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
	use SoftDeletes;

  protected $date = [
      'deleted_at'
  ];

	protected $fillable = [
    'parent_id',
		'user_id',
    'assigned_to',
    'code',
		'title',
		'body',
		'status',
		'priority',
    'project_id'
	];

  public function user()
  {
    return $this->belongsTo(\App\User::class);
  }

  public function files()
  {
    return $this->hasMany(\App\Models\Tickets\TicketFile::class);
  }

  public function replies()
  {
    return $this->hasMany(\App\Models\Tickets\Ticket::class, 'parent_id', 'id');
  }

  public function getCreatedAtAttribute($value)
  {
    return Carbon::createFromTimeStamp(strtotime($value))->diffForHumans();
  }
}
