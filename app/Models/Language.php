<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Language extends Model
{
  // use SoftDeletes;

	protected $fillable = [
    'code',
    'code_long',
		'language'
	];
}
