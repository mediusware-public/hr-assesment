<?php

namespace App\Models\Membership;

use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = [
        'name',
        'price',
        'description',
        'currency',
        'interval',
        'product_id',
        'active'
    ];

    public function product() {
      return $this->belongsTo(Product::class);
    }

}
