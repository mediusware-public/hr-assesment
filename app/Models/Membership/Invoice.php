<?php

namespace App\Models\Membership;

use App\Models\Company\Company;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
  protected $fillable = [
    'payment_status',
    'billing_month',
    'billing_year',
    'billing_amount',
    'company_id',
    'created_by'
  ];

  public function user()
  {
    return $this->belongsTo(User::class, 'created_by');
  }

  public function company()
  {
    return $this->belongsTo(Company::class, 'company_id');
  }
}
