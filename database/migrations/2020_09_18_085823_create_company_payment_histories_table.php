<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyPaymentHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_payment_histories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->constrained('companies');
            $table->foreignId('user_id')->constrained('users');
            $table->string('payment_type', 191);
            $table->string('payment_id', 191);
            $table->string('card_holder_name', 191);
            $table->string('card_number', 191)->nullable();
            $table->string('card_expiration_date', 20)->nullable();
            $table->string('card_cvc', 20)->nullable();
            $table->string('card_zip', 20)->nullable();
            $table->string('currency', 10);
            $table->string('amount', 10);
            $table->string('date_from', 30);
            $table->string('date_to', 30);
            $table->string('payment_method', 191);
            $table->string('status', 50);
            $table->timestamps();
            $table->SoftDeletes()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_payment_histories');
    }
}
