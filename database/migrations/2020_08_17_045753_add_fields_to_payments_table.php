<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->string('paypal_environment')->nullable();
            $table->string('paypal_customer_id')->nullable();
            $table->string('paypal_customer_secret')->nullable();
            $table->string('stripe_environment')->nullable();
            $table->string('stripe_publisher_key')->nullable();
            $table->string('stripe_secret_key')->nullable();
            $table->string('stripe_webhook_signing_secret_key')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            //
        });
    }
}
