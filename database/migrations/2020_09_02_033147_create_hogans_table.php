<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHogansTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('hogans', function (Blueprint $table) {
      $table->id();
      $table->string('production_link')->nullable();
      $table->string('production_halo_client_id')->nullable();
      $table->string('production_client_id')->nullable();
      $table->string('production_user_id')->nullable();
      $table->string('production_password')->nullable();

      $table->string('development_link')->nullable();
      $table->string('development_halo_client_id')->nullable();
      $table->string('development_client_id')->nullable();
      $table->string('development_user_id')->nullable();
      $table->string('development_password')->nullable();

      $table->boolean('active')->default(0)->comment('1=production, 0=development');

      $table->foreignId('company_id')->references('id')->on('companies');
      $table->foreignId('created_by')->references('id')->on('users');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('hogans');
  }
}
