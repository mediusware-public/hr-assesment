<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
          $table->id();
          $table->integer('parent_id')->nullable();
          $table->foreignId('assigned_to')->nullable()->constrained('users')->onDelete('cascade');
          $table->integer('user_id');
          $table->foreignId('project_id')->nullable()->constrained('projects');
          $table->string('code', 191)->nullable();
          $table->string('title', 191)->nullable();
          $table->text('body');
          $table->string('priority', 50)->nullable();
          $table->string('status', 20)->default('open');
          $table->timestamps();
          $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
