<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('media', function (Blueprint $table) {
      $table->id();
      $table->tinyInteger('is_private')->default(0)->comment("1=Yes, 0=No")->index();
      $table->string('title')->index()->nullable();
      $table->string('slug')->nullable();
      $table->mediumText('caption')->nullable();
      $table->mediumText('alt')->nullable();
      $table->string('path');
      $table->string('type');
      $table->mediumText('description')->nullable();
      $table->foreignId('user_id')->constrained('users');
      $table->foreignId('company_id')->constrained('companies');
      $table->integer('modified_by')->unsigned()->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('media');
  }
}
