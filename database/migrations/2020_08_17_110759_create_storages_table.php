<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoragesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('storages', function (Blueprint $table) {
            $table->id();
            $table->string('google_drive_developer_key')->nullable();
            $table->string('google_drive_client_id')->nullable();
            $table->string('google_drive_app_id')->nullable();
            $table->string('google_drive_scopes')->nullable();
            $table->string('dropbox_client_id')->nullable();
            $table->string('dropbox_client_secret')->nullable();
            $table->string('onedrive_client_id')->nullable();
            $table->string('onedrive_client_secret')->nullable();
            $table->string('s3_client_id')->nullable();
            $table->string('s3_client_secret')->nullable();
            $table->string('fatture_api_client_id')->nullable();
            $table->string('fatture_api_client_secret')->nullable();
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('storages');
    }
}
