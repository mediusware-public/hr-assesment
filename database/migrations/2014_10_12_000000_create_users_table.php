<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('photo')->nullable();
            $table->enum('title', ['Mr.', 'Mrs.', 'Miss'])->default('Mr.');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->enum('status', ['active', 'pending', 'inactive'])->default('active');
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone')->nullable();
            $table->string('participantId')->nullable();
            $table->string('participantPassword')->nullable();
            $table->text('address')->nullable();
            $table->longText('participant_note')->nullable();
            $table->string('paypal_id')->nullable();
            $table->string('fatture_id')->nullable();
            $table->string('language')->default('en');
            $table->string('timezone')->default('UTC');
            $table->enum('role', ['super_admin', 'admin', 'participant', 'hr'])->default('super_admin');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
