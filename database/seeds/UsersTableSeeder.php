<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // Create 10 records of customers
//    $faker = Faker::create();
//
//    // Create course
//    factory(\App\Models\Company\Company::class, 10)->create()->each(function ($course) {
//
//      // Create Lesson
//      factory(Lesson::class, 10)->create([
//        'course_id' => $course->id
//      ])->each(function ($lesson) use ($course) {
//
//        // Create Essay
//        factory(Essay::class, 3)->create([
//          'course_id' => $course->id,
//          'lesson_id' => $lesson->id
//        ]);
//
//      });
//
//    });

    DB::table('users')->insert([
      'title' => 'Mr.',
      'first_name' => 'Simone',
      'last_name' => 'Chinaglia',
      'email' => 'admin@gmail.com',
      'role' => 'super_admin',
      'password' => Hash::make('12345678'),
    ]);
  }
}
