<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DummyDataSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::beginTransaction();
    DB::statement("SET FOREIGN_KEY_CHECKS = 0");

    //assessments
    DB::insert("INSERT INTO `assessments` (`id`, `key`, `value`) VALUES
          (1, 'HDS', 'Hogan Development Survey'),
          (2, 'HPI', 'Hogan Personality Inventory'),
          (3, 'JUDA', 'Judgment Assessment'),
          (4, 'JUDC', 'Judgment Cognitive'),
          (5, 'JUDV', 'Judgment Verbal'),
          (6, 'MVPI', 'Motives, Values, Preferences Inventory'),
          (7, 'RAVENSAPMIII', 'Ravens Progressive Matrices®, APM-III')");

    //reports
    DB::insert("INSERT INTO `reports` (`id`, `key`, `value`) VALUES
          (1, 'RPInsightFlashPkg', 'PKG-Insight - HPI, HDS &amp; MVPI + Flash'),
          (2, 'EcHPIINSRV2', 'Insight - HPI'),
          (3, 'EcHDSINSRV2', 'Insight - HDS'),
          (4, 'EcMVPIINSRV2', 'Insight - MVPI'),
          (5, 'EcHPIHDSMVPIFR', 'Flash'),
          (6, 'EcJUDCJUDVJUDAJR', 'Judgement - General'),
          (7, 'EcJUDCJUDVJUDAJR', 'Judgement - Global'),
          (8, 'EcRAVENAPM', 'Raven\'s Progressive Matrices®, APM-III'),
          (9, 'RPTINSIGHTJUDGMENTFLASH', 'PKG-Judgment + Insight Reports + Flash')");

    //products
    $products = [
      ['name' => 'galileo', 'code' => 'prod_HtPxAPP0tsEXUT', 'type' => 'service']
    ];
    foreach ($products as $product) {
      \App\Models\Product\Product::create($product);
    }
    //job_lavels
    $user_id = \App\User::first()->id;
    $job_lavels = [
      ['name' => 'Executive', 'user_id' => $user_id],
      ['name' => 'Middle manager', 'user_id' => $user_id],
      ['name' => 'Entry-level supervisor', 'user_id' => $user_id],
      ['name' => "Individual contributor", 'user_id' => $user_id]
    ];

    foreach ($job_lavels as $job_lavel) {
      \App\Models\Participant\JobLavel::create($job_lavel);
    }
//job_categories
    $job_categories = [
      ['name' => 'Management and executives', 'user_id' => $user_id],
      ['name' => 'Professionals', 'user_id' => $user_id],
      ['name' => 'Technicians and specialists', 'user_id' => $user_id],
      ['name' => 'Operations and trades', 'user_id' => $user_id],
      ['name' => 'Sales', 'user_id' => $user_id],
      ['name' => 'Customer support', 'user_id' => $user_id],
      ['name' => 'Administrative and clerical', 'user_id' => $user_id],
      ['name' => 'Service and support', 'user_id' => $user_id],
      ['name' => 'Military', 'user_id' => $user_id],
      ['name' => 'Student', 'user_id' => $user_id]
    ];

    foreach ($job_categories as $job_category) {
      \App\Models\Participant\JobCategory::create($job_category);
    }

//$job_ethnicitys
    $job_ethnicitys = [
      ['name' => 'Two or More Races', 'user_id' => $user_id],
      ['name' => 'Black or African American', 'user_id' => $user_id],
      ['name' => 'Hispanic or Latino', 'user_id' => $user_id],
      ['name' => 'Asian', 'user_id' => $user_id],
      ['name' => 'American Indian or Alaska Native', 'user_id' => $user_id],
      ['name' => 'White', 'user_id' => $user_id],
      ['name' => 'Not Indicated', 'user_id' => $user_id],
      ['name' => 'Declined', 'user_id' => $user_id],
      ['name' => 'Native Hawaiian or Pacific Islander', 'user_id' => $user_id],
      ['name' => 'Others', 'user_id' => $user_id]
    ];
    foreach ($job_ethnicitys as $job_ethnicity) {
      \App\Models\Participant\JobEthnicity::create($job_ethnicity);
    }
//$job_assessment_purposes
    $job_assessment_purposes = [
      ['name' => 'Applying for a job', 'user_id' => $user_id],
      ['name' => 'Part of my current job', 'user_id' => $user_id],
      ['name' => 'Other', 'user_id' => $user_id]
    ];

    foreach ($job_assessment_purposes as $job_assessment_purpose) {
      \App\Models\Participant\JobAssessmentPurpose::create($job_assessment_purpose);
    }
//$job_industries
    $job_industries = [
      ['name' => 'Advertising and Marketing', 'user_id' => $user_id],
      ['name' => 'Banking and Financial Services', 'user_id' => $user_id],
      ['name' => 'Business Support Services', 'user_id' => $user_id],
      ['name' => 'Construction', 'user_id' => $user_id],
      ['name' => 'Education', 'user_id' => $user_id],
      ['name' => 'Energy, Utilities, and Telecommunications', 'user_id' => $user_id],
      ['name' => 'Entertainment and Media', 'user_id' => $user_id],
      ['name' => 'Food and Beverage', 'user_id' => $user_id],
      ['name' => 'Government', 'user_id' => $user_id],
      ['name' => 'Health Care', 'user_id' => $user_id],
      ['name' => 'Industrial Metals and Mining', 'user_id' => $user_id],
      ['name' => 'Information Technology', 'user_id' => $user_id],
      ['name' => 'Law Enforcement', 'user_id' => $user_id],
      ['name' => 'Leisure and Hospitality', 'user_id' => $user_id],
      ['name' => 'Manufacturing', 'user_id' => $user_id],
      ['name' => 'Pharmaceuticals', 'user_id' => $user_id],
      ['name' => 'Retail Sales', 'user_id' => $user_id],
      ['name' => 'Sports and Recreation', 'user_id' => $user_id],
      ['name' => 'Transportation', 'user_id' => $user_id],
      ['name' => 'Other', 'user_id' => $user_id]
    ];

    foreach ($job_industries as $job_industry) {
      \App\Models\Participant\JobIndustry::create($job_industry);
    }

    //
    DB::statement("SET FOREIGN_KEY_CHECKS = 1");
    DB::commit();
  }
}
