<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Tickets\Ticket;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Ticket::class, function (Faker $faker) {
  return [
    'user_id' => rand(1, 10),
    'code' => rand(1111, 9999),
    'title' => $faker->name,
    'body' => $faker->text,
    'assigned_to' => User::all()->random()->id,
    'status' => $faker->randomElement(['close', 'open', 'cancel']),
    'priority' => $faker->randomElement(['high', 'middle', 'low'])
  ];
});
