<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Company\CompanyUser;
use App\Models\Company\Company;
use App\User;

use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(CompanyUser::class, function (Faker $faker) {
  $users     = User::query()->whereIn('role', ['admin', 'hr', 'participant'])->get()->count();
  $companies = Company::all()->count();
  return [
    'user_id'    => $faker->unique()->numberBetween(1, $users),
    'company_id' => rand(1, $companies)
  ];
});
