<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Company\CompanyPlan;
use App\Models\Company\Company;
use App\Models\Membership\Plan;

use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(CompanyPlan::class, function (Faker $faker) {
  $plans     = Plan::all()->count();
  $companies = Company::all()->count();
  return [
    'company_id' => $faker->unique()->numberBetween(1, $companies),
    'plan_id'    => rand(1, $plans)
  ];
});
