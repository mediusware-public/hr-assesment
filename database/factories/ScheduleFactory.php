<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Schedules\Schedule;
use Faker\Generator as Faker;

$factory->define(Schedule::class, function (Faker $faker) {
    return [
      'title'			        => $faker->name,
      'description'			  => $faker->text,
      'date'			        => $faker->dateTime,
      'address'			      => $faker->address,
      'repeat'		        => $faker->randomElement(['never', 'daily', 'weekly' , 'monthly', 'yearly']),
    ];
});
