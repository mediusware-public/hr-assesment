<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/forget-password/send-code', 'Api\AuthController@sendCode');
Route::post('/forget-password/check-code', 'Api\AuthController@checkCode');
Route::get('/forget-password/check-token/{code_token}', 'Api\AuthController@checkToken');
Route::post('/forget-password/reset-password', 'Api\AuthController@resetPassword');
Route::post('/login', 'Api\AuthController@login');
Route::get('/password/reset', 'Api\AuthController@showResetForm');
Route::post('/password/reset', 'Api\AuthController@reset')->name('api.password.update');

Route::middleware('auth:api')->group(function () {

  // Auth routes
  Route::get('/user', 'Api\AuthController@getAuthUser');
  Route::get('/logout', 'Api\AuthController@logout');
  Route::get('/login-history', 'Api\AuthController@loginHistory');
  Route::get('/login-history/{id}', 'Api\AuthController@loginHistoryByUserId');
  Route::get('/login-history/company/{company_id}', 'Api\AuthController@loginHistoryByCompanyId');
  Route::get('/logout-from-device/{id}', 'Api\AuthController@logOutFromDevice');

  Route::get('login-as/{userId}', 'Api\AuthController@loginAs');
  Route::get('login-as-otp/{userId}', 'Api\AuthController@loginAsOTP');

  // Membership
  Route::get('membership/export-pdf', 'MembershipController@exportPdf');
  Route::get('membership/export-excel', 'MembershipController@exportExcel');
  Route::match(['POST', 'GET'],'membership/subscribe/new/{id?}', 'MembershipController@newSubscribe');
  Route::match(['POST', 'GET'],'membership/subscribe/old/{id?}', 'MembershipController@oldSubscriberPayment');
  Route::apiResource('membership/plans', 'MembershipController');


  // Storage Route
  Route::apiResource('storage/api', 'StorageApiController');

  // Payment Route
  Route::apiResource('payment/api', 'PaymentApiController');

  // Company User Route
  Route::get('company/user/export-pdf', 'CompanyUserController@exportPdf');
  Route::get('company/user/export-excel', 'CompanyUserController@exportExcel');
  Route::get('company/{id}/users', 'CompanyUserController@getCompanyUsersById');
  Route::get('company/{id}/users/export-pdf', 'CompanyUserController@exportCompanyAllUsersPdf');
  Route::get('company/{id}/users/export-excel', 'CompanyUserController@exportCompanyAllUsersExcel');

  Route::get('company/{id}/participants', 'CompanyUserController@getCompanyParticipantsById');
  Route::get('company/{id}/participants/export-pdf', 'CompanyUserController@exportCompanyAllParticipantPdf');
  Route::get('company/{id}/participants/export-excel', 'CompanyUserController@exportCompanyAllParticipantExcel');


  Route::get('getCompany/user/{id}', 'CompanyUserController@getCompanyByUserId');
  Route::get('company/user-log/{id}', 'CompanyUserController@getCompanyUserLogById');
  Route::get('company/user/get-project-participant/{project_slug}', 'CompanyUserController@getProjectParticipantList');
  Route::put('/company/user/note/{id}', 'CompanyUserController@participantNoteUpdate');
  Route::apiResource('company/user', 'CompanyUserController');

  Route::get('/company/hr/change-receive-communication/{id}', 'CompanyHRController@changeAccessorCommunication');
  Route::apiResource('company/hr', 'CompanyHRController');


  // Company Route
  Route::get('company/api/export-pdf', 'CompanyController@exportPdf');
  Route::get('company/api/export-excel', 'CompanyController@exportExcel');
  Route::get('company/allCompanies', 'CompanyController@getAllCompanies');
  Route::get('company/api/get-single-company', 'CompanyController@getSingleCompany');
  Route::put('company/api/update-single-company', 'CompanyController@updateSingleCompany');
  Route::get('company/api/get-company-communication-receiver/{id}', 'CompanyController@getCompanyCommunicationReceiver');
  Route::apiResource('company/api', 'CompanyController');


  // Ticket Route
  Route::get('ticket/super-admin', 'TicketController@getAllSuperAdmin');
  Route::get('ticket/export-pdf', 'TicketController@exportPdf');
  Route::get('ticket/export-excel', 'TicketController@exportExcel');
  Route::apiResource('ticket', 'TicketController');

  // Ticket Replay Route
  Route::apiResource('/ticket/replay', 'TicketReplayController');

  // Ticket File Delete
  Route::delete('/ticket/file/{id}', 'TicketController@deleteFile');

  // Get HOGAN access_token
  Route::get('project/get-access-token', 'ProjectController@getAccessToken');
  // Project Route
  Route::apiResource('/project', 'ProjectController');

  Route::get('/company-assessment/goto-participant-test-url/{key}', 'CompanyAssessmentController@gotoParticipantTesturl');
  Route::get('/company-assessment/get-participant-assessment/{participant_id}', 'CompanyAssessmentController@getParticipantAssessment');
  Route::get('/company-assessment/{company_id}/{project_id}', 'CompanyAssessmentController@getProjectAssessments');
  Route::get('/company-assessment/get-all-assessments', 'CompanyAssessmentController@getAllAssessments');
  // Company Assessment Route
  Route::post('/company-assessment/assign-participant-assessment', 'CompanyAssessmentController@assignParticipantAssessment');
  Route::post('/company-assessment/assign-participant-assessment/by-participant-checkbox', 'CompanyAssessmentController@assignParticipantAssessmentBycheckboxSelect');
  Route::apiResource('/company-assessment', 'CompanyAssessmentController');

  Route::get('/company-report/{company_id}/{project_id}', 'CompanyReportController@getProjectReports');
  Route::get('/company-report/get-all-reports', 'CompanyReportController@getAllReports');
  // Company Report Route
  Route::apiResource('/company-report', 'CompanyReportController');

  //Mail Route
  Route::get('/mail/participant-communication/export-pdf/{participant_id}', 'MailController@exportParticipantCommunicationPdf');
  Route::get('/mail/participant-communication/export-excel/{participant_id}', 'MailController@exportParticipantCommunicationExcel');
  Route::get('/mail/participant-list/{participant_id}', 'MailController@getParticipantCommunicationHistory');

  Route::get('/mail/superadmin-company-communication-list/export-pdf/{company_id}', 'MailController@exportSuperadminCompanyCommunicationListPdf');
  Route::get('/mail/superadmin-company-communication-list/export-excel/{company_id}', 'MailController@exportSuperadminCompanyCommunicationListExcel');
  Route::get('/mail/superadmin-company-communication-list/{company_id}', 'MailController@getSuperadminCompanyCommunicationList');


  Route::get('/mail/company-project-communication-list/export-pdf/{project_slug}', 'MailController@exportCompanyProjectCommunicationHistoryPdf');
  Route::get('/mail/company-project-communication-list/export-excel/{project_slug}', 'MailController@exportCompanyProjectCommunicationHistoryExcel');
  Route::get('/mail/company-project-communication-list/{project_slug}', 'MailController@getCompanyProjectCommunicationHistory');
  Route::get('/inbox-mail', 'MailController@inboxMail');
  Route::post('/mail/send-multiple-participant-email', 'MailController@sendMultipleParticipantEmail');
  Route::apiResource('/mail', 'MailController');


  // TemplateBuilder Route
  Route::post('/template/export-pdf', 'TemplateBuilderController@exportPdf');

  // User Profile Update Route
  Route::put('/profile/update-profile', 'ProfileController@update');

  // All Users Route
  Route::get('/user/get-intent/{id}', 'UserController@getIntent');
  Route::get('/users/roleWiseUsers', 'UserController@roleWiseUsers');
  Route::get('/users/user-details/{id}', 'UserController@show');

  Route::get('/users/user', 'UserController@index');
  Route::get('/users/admin', 'UserController@getAdminUsers');
  Route::get('/users/hr', 'UserController@getHrUsers');
  Route::get('/users/super-admin', 'UserController@getSuperAdminUsers');
  Route::post('/users/import', 'UserController@import');
  Route::post('/users/send-credentials', 'UserController@sendCredentials');

  //Media
  Route::post('cloud-file-picker', 'MediaController@cloudFilePicker');
  Route::get('media/select-media/{id}', 'MediaController@selectMedia');
  Route::apiResource('/media', 'MediaController');



  //Custom Payment Route
  Route::apiResource('custom-payment', 'CustomPaymentController');

  //Schedule Route
  Route::apiResource('schedule', 'ScheduleController');


  //Hogan Route
  Route::apiResource('hogan', 'HoganController');

  //Dashboard
  Route::get('dashboard', 'DashboardController@index');


  // Company Payment History
  Route::get('/company-payment-history/send-invoice/{payment_id}', 'CompanyPaymentHistoryController@sendInvoice');

  //for fake paypal purpose
  Route::post('/company-payment-history/send-invoice', 'CompanyPaymentHistoryController@sendInvoice');
  //for fake paypal purpose

  //CompanyPaymentHistory
  Route::apiResource('/company-payment-history', 'CompanyPaymentHistoryController');

  //CompanyPaymentDefault
  Route::apiResource('/company-payment-default', 'CompanyPaymentDefaultController');

  //Language
  Route::apiResource('/language', 'LanguageController');

});
