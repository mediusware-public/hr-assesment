const reportRoute = [
{
    path: '/report',
    name: 'report',
    component: () => import('./components/Report.vue'),
    meta: {
        requireAuth: true,
        breadcrumb: [
            { title: 'Home', url: '/' },
            { title: 'Report', active: true }
        ],
        title: 'Report',
    }
}
]

export default reportRoute;