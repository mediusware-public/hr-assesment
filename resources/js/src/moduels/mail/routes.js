const sendmailRoute = [
    {
        path: '/mail',
        name: 'Mail',
        component: () => import('./Mail.vue'),
        meta: {
          requireAuth: true,
          title: 'Mail'
        },
    },
]

export default sendmailRoute;
