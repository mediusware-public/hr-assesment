const helpRoute = [
    {
        path: '/help',
        name: 'Help',
        component: () => import('./components/Help.vue'),
        meta: {
            requireAuth: true,
            breadcrumb: [
                { title: 'Home', url: '/' },
                { title: 'Help', active: true }
            ],
            title: 'Help',
        }
    },
    {
        path: '/help/category',
        name: 'HelpCategory',
        component: () => import('./components/HelpCategory.vue'),
        meta: {
            requireAuth: true,
            breadcrumb: [
                { title: 'Home', url: '/' },
                { title: 'Help', url: '/help' },
                { title: 'Category', active: true }
            ],
            title: 'Help',
            // parent: 'help'
        }
    },
    {
        path: '/help/category/question',
        name: 'HelpCategoryQuestion',
        component: () => import('./components/HelpCategoryQuestion.vue'),
        meta: {
            requireAuth: true,
            breadcrumb: [
                { title: 'Home', url: '/' },
                { title: 'Help', url: '/help' },
                { title: 'Category', url: '/help/category' },
                { title: 'Question', active: true }
            ],
            title: 'Help',
            // parent: 'help'
        }
    },
]

export default helpRoute;