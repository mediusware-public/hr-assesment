const permissionRoute = [
{
    path: '/permission',
    name: 'Permission',
    component: () => import('./components/Permission.vue'),
    meta: {
        requireAuth: true,
        breadcrumb: [
            { title: 'Home', url: '/' },
            { title: 'Permission', active: true }
        ],
        title: 'Permission',
    }
}
]

export default permissionRoute;