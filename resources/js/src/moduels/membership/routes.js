const membershipRoute = [
    {
      path: '/membership/plans/view-plans',
      name: 'ViewPlans',
      component: () => import('./plans/ViewPlans.vue'),
      meta: {
        requireAuth: true,
        breadcrumb: [
          { title: 'Home', url: '/' },
          { title: 'Plans', active: true }
        ],
        title: 'Plan',
      }
    },

    {
      path: '/membership/plans/add-plans',
      name: 'AddPlan',
      component: () => import('./plans/AddPlan.vue'),
      meta: {
        requireAuth: true,
        breadcrumb: [
          { title: 'Home', url: '/' },
          { title: 'Plans', url: '/membership/plans/view-plans' },
          { title: 'Add Plan', active: true }
        ],
        title: 'Plan',
      }
    },

    {
      path: '/membership/payments/bank-payments',
      name: 'BankPayments',
      component: () => import('./payments/BankPayments.vue'),
      meta: {
        requireAuth: true,
        breadcrumb: [
          { title: 'Home', url: '/' },
          { title: 'Payments', url: '/membership/payments/bank-payments' },
          { title: 'Bank Payments', active: true }
        ],
        title: 'Bank Payments',
      }
    },

    {
      path: '/membership/payments',
      name: 'Payments',
      component: () => import('./payments/PaymentOverview.vue'),
      meta: {
        requireAuth: true,
        breadcrumb: [
          { title: 'Home', url: '/' },
          { title: 'Payments', active: true }
        ],
        title: 'Payments',
      }
    },

    {
      path: '/membership/payments/stripe',
      name: 'StripePayments',
      component: () => import('./payments/stripe/SubscriptionManagement.vue'),
      meta: {
        requireAuth: true,
        breadcrumb: [
          { title: 'Home', url: '/' },
          { title: 'Payments', url: '/membership/payments' },
          { title: 'Stripe', active: true }
        ],
        title: 'Stripe',
      }
    },

    {
      path: '/membership/payments/paypal',
      name: 'PayPalPayments',
      component: () => import('./payments/fake_paypal/SubscriptionManagement'),
      meta: {
        requireAuth: true,
        breadcrumb: [
          { title: 'Home', url: '/' },
          { title: 'Payments', url: '/membership/payments' },
          { title: 'PayPal', active: true }
        ],
        title: 'PayPal',
      }
    },
    {
      path: '/membership/payments/fatture',
      name: 'FatturePayments',
      component: () => import('./payments/fake_fatture/SubscriptionManagement'),
      meta: {
        requireAuth: true,
        breadcrumb: [
          { title: 'Home', url: '/' },
          { title: 'Payments', url: '/membership/payments' },
          { title: 'Fatture', active: true }
        ],
        title: 'Fatture',
      }
    }
]

export default membershipRoute;
