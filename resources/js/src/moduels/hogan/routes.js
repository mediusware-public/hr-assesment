const hoganRoute = [
  {
    path: "/hogan",
    name: "ViewHogan",
    component: () => import("./component/Hogan.vue"),
    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "API Hogan Management", active: true },
      ],
      title: "Hogan"
    }
  }
];

export default hoganRoute;
