const settingsRoute = [
  {
    path: "/settings/payment/add-payment-api",
    name: "AddPaymentApi",
    component: () => import("./payment/AddPaymentApi.vue"),
    meta: {
      requireAuth: true,
      title: "Add Payment Api"
    }
  },
  {
    path: "/settings/storage/add-storage-api",
    name: "AddStorageApi",
    component: () => import("./storage/AddStorageApi.vue"),
    meta: {
      requireAuth: true,
      title: "Add Storage Api"
    }
  },
  {
    path: "/settings/zoom/add-zoom-api",
    name: "AddZoomApi",
    component: () => import("./zoom/AddZoomApi.vue"),
    meta: {
      requireAuth: true,
      title: "Add Zoom Api"
    }
  }
];

export default settingsRoute;
