const usermanagementRoute = [
  {
    path: '/user-management',
    name: 'User',
    component: () => import('./components/SelectUsersView.vue'),
    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: 'Home', url: '/' },
        { title: 'User Management', active: true }
      ],
      title: 'User Management',
    }
  },
  {
    path: '/user-management/add',
    name: 'AddParticipant',
    component: () => import('./components/AddCompanyUser.vue'),
    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: 'Home', url: '/' },
        { title: 'User Management', url: '/user-management' },
        { title: 'Add', active: true }
      ],
      title: 'User Management',
    }
  },

  {
    path: '/users',
    name: 'UsersList',
    component: () => import('./components/UsersList.vue'),
    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: 'Home', url: '/' },
        { title: 'User Management', url: '/user-management' },
        { title: 'User', active: true }
      ],
      title: 'User',
    }
  },

  {
    path: '/project/:projectSlug/participants/:participantId',
    name: 'ParticipantView',
    component: () => import('./components/participant/ParticipantView.vue'),
    meta: {
      requireAuth: true,
      // breadcrumb: [
      //   { title: 'Home', url: '/' },
      //   { title: 'Users', url: '/users' },
      //   { title: 'User', active: true }
      // ],
      // title: 'User',
    }
  },

  {
    path: '/company-users/:companyUserId',
    name: 'CompanyUser',
    component: () => import('./components/HR/AccessorDetails/AccessorDetailsTabComponent'),
    meta: {
      requireAuth: true,
      // breadcrumb: [
      //   { title: 'Home', url: '/' },
      //   { title: 'Accessor', active: true }
      // ],
      title: 'User Details',
    }
  },

]

export default usermanagementRoute;
