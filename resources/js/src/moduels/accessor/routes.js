const accessorRoute = [
  {
    path: "/",
    name: "UsersList",
    component: () => import("./Accessor.vue"),
    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Users", active: true }
      ],
      title: "Users"
    }
  },
];

export default accessorRoute;
