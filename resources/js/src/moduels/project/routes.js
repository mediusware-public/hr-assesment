const projectRoute = [
  {
      path: '/project',
      name: 'Project',
      component: () => import('./components/Overview.vue'),
      meta: {
          requireAuth: true,
          breadcrumb: [
              { title: 'Home', url: '/' },
              { title: 'Project', active: true }
          ],
          title: 'Project',
      }
  },
  {
      path: '/project/add',
      name: 'Add Project',
      component: () => import('./components/OpenProject.vue'),
      meta: {
          requireAuth: true,
          breadcrumb: [
              { title: 'Home', url: '/' },
              { title: 'Project', url: '/project' },
              { title: 'Add Project', active: true }
          ],
          title: 'Project',
      }
  },
  {
    path: "/project/:projectSlug",
    name: "Project Details",
    component: () => import("./components/ProjectParent.vue"),
    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Project", url: "/project" },
        { title: "Project Details", active: true }
      ],
      title: "Project"
    }
  },
  {
    path: "/project/:projectSlug/:apiProvider",
    name: "Project Details",
    component: () => import("./components/ViewProjectManagement.vue"),
    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Project", url: "/project" },
        { title: "Project Details", active: true }
      ],
      title: "Project"
    }
  },
  {

    path: "/ticket/:projectSlug/edit",
    name: "Edit Project Details",
    component: () => import("./components/EditProject.vue"),

    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Project", url: "/ticket" },
        { title: "Edit Project Details", active: true }
      ],
      title: "Project"
    }
  },
]

export default projectRoute;
