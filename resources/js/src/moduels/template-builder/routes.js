const templatebuilderRoute = [
    {
        path: '/template/template-builder',
        name: 'TemplateBuilder',
        component: () => import('./components/TemplateBuilder.vue'),
        meta: {
            requireAuth: true,
            breadcrumb: [
                { title: 'Home', url: '/' },
                { title: 'Template', url: '/template/template-builder' },
                { title: 'Template Builder', active: true }
            ],
            pageTitle: 'Template Builder',
        }
    }
]

export default templatebuilderRoute;
