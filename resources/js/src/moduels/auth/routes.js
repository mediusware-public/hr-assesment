const authRoute = [
    {
        path: '/login',
        name: 'login',
        component: () => import('./Login.vue'),
        meta: {
            title: 'Login'
        }
    },
    {
        path: '/forget-password',
        name: 'forgetPassword',
        component: () => import('./ForgetPassword.vue'),
        meta: {
            title: 'Forget My Password'
        }
    },
    {
        path: '/forget-password/:code_token',
        name: 'forgetPassword',
        component: () => import('./ForgetPassword.vue'),
        meta: {
            title: 'Forget My Password'
        }
    },
    {
        path: '/password/reset',
        name: 'PasswordReset',
        component: () => import('./PasswordReset.vue'),
        meta: {
            title: 'Password Reset'
        }
    }
]

export default authRoute;
