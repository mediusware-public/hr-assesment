const authAsRoute = [
    {
        path: '/login-as/user/:userId/:action?',
        name: 'loginAs',
        component: () => import('./LoginAs.vue'),
        meta: {
            title: 'Login As'
        }
    },
]

export default authAsRoute;
