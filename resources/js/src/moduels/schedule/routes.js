const scheduleRoute = [
{
    path: '/schedule',
    name: 'Schedule',
    component: () => import('./components/Schedule.vue'),
    meta: {
        requireAuth: true,
        breadcrumb: [
            { title: 'Home', url: '/' },
            { title: 'Schedule', active: true }
        ],
        title: 'Schedule',
    }
}
]

export default scheduleRoute;