const carRoute = [
  {
    path: "/modules/car",
    name: "CAREER ACHIEVEMENT RECORD",
    component: () => import("./modules/Car.vue"),
    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Career achivement record", active: true },
      ],
      title: "Career achivement record"
    }
  }
];

export default carRoute;
