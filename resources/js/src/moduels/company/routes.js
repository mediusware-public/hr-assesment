const companyRoute = [
  {
    path: "/company",
    name: "ViewCompanies",
    component: () => import("./component/ViewCompanies.vue"),
    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Company", active: true }
      ],
      title: "Company"
    }
  },
  {
    path: "/company/add",
    name: "AddCompany",
    component: () => import("./component/AddCompany.vue"),
    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Company", url: "/company" },
        { title: "Add Company", active: true }
      ],
      title: "Company"
    }
  },
  {
    path: "/company/:companyId",
    name: "CompanyDetails",
    component: () => import("./component/CompanyParent.vue"),
    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Company", url: "/company" },
        { title: "Company Details", active: true }
      ],
      title: "Company"
    }
  },
  {
    path: "/company/:companyId/edit",
    name: "EditCompanyDetails",
    component: () => import("./component/CompanyParent.vue"),
    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Company", url: "/company" },
        { title: "Company Details", active: true }
      ],
      title: "Company"
    }
  },
  {
    path: "/company/:companyId/user",
    name: "CompanyUsers",
    component: () => import("./component/CompanyParent.vue"),
    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Company", url: "/company" },
        { title: "Company Details", active: true }
      ],
      title: "Company"
    }
  },


  {
    path: "/company/:companyId/participant/:participantId",
    name: "CompanyParticipant",
    component: () => import("../user-management/components/participant/ParticipantView.vue"),
    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Company", url: "/company" },
        { title: "Participant Details", active: true }
      ],
      title: "Participant Details"
    }
  },


  {
    path: "/company/view-companies",
    name: "ViewCompanies",
    component: () => import("./component/ViewCompanies.vue"),
    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Company", url: "/company/view-companies" },
        { title: "View Companies", active: true }
      ],
      title: "Company"
    }
  }
];

export default companyRoute;
