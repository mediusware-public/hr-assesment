const profileRoute = [
    {
        path: '/profile/view-profile',
        name: 'ViewProfile',
        component: () => import('./profilemenu/Profile.vue'),
        meta: {
            requireAuth: true,
            breadcrumb: [
                { title: 'Home', url: '/' },
                { title: 'Profile', url: '/profile/view-profile' },
                { title: 'View Profile', active: true }
            ],
            pageTitle: 'View Profile',
        }
    }
]

export default profileRoute;