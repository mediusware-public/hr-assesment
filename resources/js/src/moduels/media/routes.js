const MediaRoute = [
    {
        path: '/media',
        name: 'Media',
        component: () => import('./components/Media.vue'),
        meta: {
            requireAuth: true,
            breadcrumb: [
                { title: 'Home', url: '/' },
                { title: 'File Manager', active: true }
            ],
            title: 'File Manager',
        }
    }
]

export default MediaRoute;
