const ticketRoute = [
  {
      path: '/support',
      name: 'Ticket',
      component: () => import('./ticketmenu/components/Overview.vue'),
      meta: {
          requireAuth: true,
          breadcrumb: [
              { title: 'Home', url: '/' },
              { title: 'Ticket', active: true }
          ],
          title: 'Support',
      }
  },
  {
      path: '/support/add',
      name: 'Add Ticket',
      component: () => import('./ticketmenu/components/OpenTicket.vue'),
      meta: {
          requireAuth: true,
          breadcrumb: [
              { title: 'Home', url: '/' },
              { title: 'Ticket', url: '/support' },
              { title: 'Add Ticket', active: true }
          ],
          title: 'Support',
      }
  },
  {
    path: "/support/:supportId",
    name: "Ticket Details",
    component: () => import("./ticketmenu/components/TicketParent.vue"),
    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Ticket", url: "/support" },
        { title: "Ticket Details", active: true }
      ],
      title: "Ticket"
    }
  },
  {

    path: "/support/:supportId/edit",
    name: "Edit Ticket Details",
    component: () => import("./ticketmenu/components/EditTicket.vue"),

    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Ticket", url: "/support" },
        { title: "Edit Ticket Details", active: true }
      ],
      title: "Ticket"
    }
  },
  {

    path: "/support/:supportId/replay",
    name: "Replay in Ticket",
    component: () => import("./ticketmenu/components/ReplayTicket.vue"),

    meta: {
      requireAuth: true,
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Ticket", url: "/support" },
        { title: "Replay in Ticket", active: true }
      ],
      title: "Ticket"
    }
  },
]

export default ticketRoute;
