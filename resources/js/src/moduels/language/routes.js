const languageRoute = [
{
    path: '/language',
    name: 'Language',
    component: () => import('./components/LanguageList.vue'),
    meta: {
        requireAuth: true,
        breadcrumb: [
            { title: 'Home', url: '/' },
            { title: 'Language', active: true }
        ],
        title: 'Language List',
    },
},
  {
    path: '/language/add',
    name: 'Add Language',
    component: () => import('./components/SingleLanguage.vue'),
    meta: {
      requireAuth: true,
      breadcrumb: [
        {title: 'Home', url: '/'},
        { title: 'Language', url: '/language' },
        { title: 'Add Language', active: true }
      ],
      title: 'Add Language',
    },
  },
  {
    path: '/language/:languageCode/edit',
    name: 'EditLanguage',
    component: () => import('./components/SingleLanguage.vue'),
    meta: {
      requireAuth: true,
      breadcrumb: [
        {title: 'Home', url: '/'},
        { title: 'Language', url: '/language' },
        { title: 'Edit Language', active: true }
      ],
      title: 'Edit Language',
    },
  }

]

export default languageRoute;
