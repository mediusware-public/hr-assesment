import Vue from 'vue';
Vue.mixin({
  methods: {
    fileType: function (file){
      let fileType = file.split('.');
      fileType = ( fileType[fileType.length-1]).toLowerCase();
      switch (fileType) {
      case 'pdf':
        return 'pdf';
        break;

      case 'doc':
        return 'doc';
        break;

      case 'docx':
        return 'docx';
        break;

      case 'xls':
        return 'xls';
        break;

      case 'xlsx':
        return 'xlsx';
        break;

      case 'csv':
        return 'csv';
        break;

      default:
        return 'image';
      }
    },

    fileDownload: function (value) {
      let fileLink = document.createElement('a');
      fileLink.href = value;
      let fileType = value.split('.');
      fileType = ( fileType[fileType.length-1]).toLowerCase();
      fileLink.setAttribute('download', 'ParticipantList.'+fileType);
      document.body.appendChild(fileLink);
      fileLink.click();
      fileLink.remove();
    }
  },
})
