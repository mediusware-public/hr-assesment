const communicationVariables =
{
  "full_name":{
    "value":"{{ $full_name }}",
    "name":"Full Name",
    "icon":"icon1",
  },
  "address":{
    "value":"{{ $address }}",
    "name":"Address",
    "icon":"icon2",
  },
};

export { communicationVariables };
