/*=========================================================================================
  File Name: main.js
  Description: main vue(js) file
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

//lodash
import verify_auth from "./services/auth.service";

window._ = require('lodash');
window.moment = require('moment');

import Vue from 'vue'

//Form Validation
import Vuelidate from 'vuelidate'
// an EventHub to share events between components
Vue.prototype.$hub = new Vue();
Vue.use(Vuelidate)

import App from './App.vue'


import ApiService from "./services/api.service";
import * as JwtService from "./services/jwt.service";


ApiService.init()

// Vuesax Component Framework
import Vuesax from 'vuesax'

Vue.use(Vuesax)


// axios
import axios from './axios.js'

Vue.prototype.$http = axios

// Filters
import './filters/filters.js'


// Theme Configurations
import '../themeConfig.js'


// Globally Registered Components
import './globalComponents.js'


// Vue Router
import router from './router'

router.beforeEach((to, from, next) => {

    if (to.matched.some(record => record.meta.requireAuth)) {
        if (!JwtService.getToken()) {
            next({
                name: 'login',
                params: {nextUrl: to.fullPath}
            })
        } else {
            //console.log('verify auth')
            ApiService.get('/user').then(response => {
                next()
            }).catch(error => {
                JwtService.destroyToken();
                next({name: 'login'})
            })
            // TODO : need to check the token on each route change
            // next();
        }
    }

    if (to.name == 'login') {
        if (JwtService.getToken()) {
            next('/');
        }
    }

    next();
    Vue.nextTick(() => {
        document.title = `${to.meta.title} - Galileo` || 'Galileo'
    })
})


// Vuex Store
import store from './store/store'


// Vuejs - Vue wrapper for hammerjs
import {VueHammer} from 'vue2-hammer'

Vue.use(VueHammer)

//toastr include
import VueToastr from '@deveodk/vue-toastr';
import '@deveodk/vue-toastr/dist/@deveodk/vue-toastr.css';

Vue.use(VueToastr);

//loading include
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';

Vue.use(Loading);
Vue.component('loading', Loading)

// PrismJS
import 'prismjs'
import 'prismjs/themes/prism-tomorrow.css'
//Global Mixins Methods
import './globalMixins.js';

//Global Filter Methods
import './globalFilters'

// Vue select css
// Note: In latest version you have to add it separately
// import 'vue-select/dist/vue-select.css';

// Language

import VueI18n from 'vue-i18n'
Vue.use(VueI18n)
const i18n = new VueI18n({
    locale: 'en',
    messages: {
        en: {}
    }
})

import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

Vue.config.productionTip = false

new Vue({
    i18n,
    router,
    store,
    render: h => h(App)
}).$mount('#app')
