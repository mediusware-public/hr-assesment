/*=========================================================================================
  File Name: moduleEcommerceActions.js
  Description: Ecommerce Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import ApiService from "../../services/api.service";

export default {
  getMediaItem(context, payload) {
    ApiService.get(`/media?page=${payload}`).then(response => {
      if (payload == 1) {
        this.files = []
      }
      let res_files = this.files
      response.data.data.forEach(data => {
        res_files.push(data)
      })
      this.pagination = Object.assign({}, response.data, {data: ''})
      this.pause_next_scroll = false
      context.commit("GET_MEDIA_ITEM", res_files) //articles will be run from mutation
    }).catch(error => {
      console.log(error)
    })
  }

}
