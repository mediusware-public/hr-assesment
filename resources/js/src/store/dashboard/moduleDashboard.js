/*=========================================================================================
  File Name: moduleecommerce.js
  Description: ecommerce Module
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './moduleDashboardState.js'
import getters from './moduleDashboardGetters.js'
import actions from './moduleDashboardActions.js'
import mutations from './moduleDashboardMutations.js'

export default {
  state,
  mutations,
  actions,
  getters
}
