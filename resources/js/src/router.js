/*=========================================================================================
  File Name: router.js
  Description: Routes for vue-router. Lazy loading is enabled.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import Router from 'vue-router'
import authRoute from "./moduels/auth/routes";
import authAsRoute from "./moduels/auth-as/routes";
import membershipRoute from "./moduels/membership/routes";
import settingsRoute from "./moduels/settings/routes";
import companyRoute from "./moduels/company/routes";
import ticketRoute from "./moduels/ticket/routes";
import projectRoute from "./moduels/project/routes";
import permissionRoute from "./moduels/permission/routes";
import helpRoute from "./moduels/help/routes";
import reportRoute from "./moduels/report/routes";
import usermanagementRoute from "./moduels/user-management/routes";
import languageRoute from "./moduels/language/routes";
import scheduleRoute from "./moduels/schedule/routes";
import profileRoute from "./moduels/profile/routes";
import MediaRoute from "./moduels/media/routes";
import templatebuilderRoute from "./moduels/template-builder/routes";
import sendmailRoute from "./moduels/mail/routes";
import hoganRoute from "./moduels/hogan/routes";
import CarRoute from "./moduels/Participant/routes";
import accessorRoute from "./moduels/accessor/routes";

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: '/',
    scrollBehavior() {
        return {x: 0, y: 0}
    },
    routes: [

        // =============================================================================
        // MAIN LAYOUT ROUTES
        // =============================================================================
        {
          path: '',
          component: () => import('./layouts/main/Main.vue'),
          children: [
            {
              path: '/',
              name: 'home',
              component: () => import('./views/Home.vue'),
              meta: {
                requireAuth: true,
                title: 'Dashboard'
              }
            },

            {
              path: '/participant/participant-select-test',
              name: 'ParticipantSelectTest',
              component: () => import('./moduels/dashboards/ParticipantSelectTestComponent'),
              meta: {
                requireAuth: true,
                title: 'Select Test'
              }
            },
          ],
        },
        // =============================================================================
        // FULL PAGE LAYOUTS
        // =============================================================================
        {
            path: '',
            component: () => import('@/layouts/full-page/FullPage.vue'),
            children: authRoute
        },

        // =============================================================================
        // Auth As Routes
        // =============================================================================
        {
            path: '/login-as',
            component: () => import('./layouts/main/Main.vue'),
            children: authAsRoute,
        },

        // =============================================================================
        // Membership Routes
        // =============================================================================
        {
            path: '/membership',
            component: () => import('./layouts/main/Main.vue'),
            children: membershipRoute,
        },


        // =============================================================================
        // Company Route
        // =============================================================================
        {
            path: '/company',
            component: () => import('./layouts/main/Main.vue'),
            children: companyRoute
        },


        // =============================================================================
        // Ticket Route
        // =============================================================================
        {
            path: '/ticket',
            component: () => import('./layouts/main/Main.vue'),
            children: ticketRoute
        },


        // =============================================================================
        // Project Route
        // =============================================================================
        {
            path: '/project',
            component: () => import('./layouts/main/Main.vue'),
            children: projectRoute
        },


        // =============================================================================
        // Permission Route
        // =============================================================================
        {
            path: '/permission',
            component: () => import('./layouts/main/Main.vue'),
            children: permissionRoute
        },


        // =============================================================================
        // Help Route
        // =============================================================================
        {
            path: '/help',
            component: () => import('./layouts/main/Main.vue'),
            children: helpRoute
        },


        // =============================================================================
        // Report Route
        // =============================================================================
        {
            path: '/report',
            component: () => import('./layouts/main/Main.vue'),
            children: reportRoute
        },


        // =============================================================================
        // User Management Route
        // =============================================================================
        {
            path: '/user-management',
            component: () => import('./layouts/main/Main.vue'),
            children: usermanagementRoute
        },


        // =============================================================================
        // Language Route
        // =============================================================================
        {
            path: '/language',
            component: () => import('./layouts/main/Main.vue'),
            children: languageRoute
        },


        // =============================================================================
        // Scheduling Route
        // =============================================================================
        {
            path: '/schedule',
            component: () => import('./layouts/main/Main.vue'),
            children: scheduleRoute
        },


        // =============================================================================
        // Settings Route
        // =============================================================================
        {
            path: '/settings',
            component: () => import('./layouts/main/Main.vue'),
            children: settingsRoute
        },

        // =============================================================================
        // Profile Route
        // =============================================================================
        {
            path: '/profile',
            component: () => import('./layouts/main/Main.vue'),
            children: profileRoute
        },

        // =============================================================================
        // Media Route
        // =============================================================================
        {
            path: '/media',
            component: () => import('./layouts/main/Main.vue'),
            children: MediaRoute
        },

        // =============================================================================
        // Template-Builder Route
        // =============================================================================
        {
            path: '/template',
            component: () => import('./layouts/main/Main.vue'),
            children: templatebuilderRoute
        },

        // =============================================================================
        // Send-Mail Route
        // =============================================================================
        {
            path: '/mail',
            component: () => import('./layouts/main/Main.vue'),
            children: sendmailRoute
        },

        // =============================================================================
        // Hogan API Route
        // =============================================================================
        {
          path: '/hogan',
          component: () => import('./layouts/main/Main.vue'),
          children: hoganRoute
        },
 // =============================================================================
        // Participants Route
        // =============================================================================
        {
          path: '/modules/car',
          component: () => import('./layouts/main/Main.vue'),
          children: CarRoute
        },


      // =============================================================================
      // Accessor API Route
      // =============================================================================
      {
        path: '/company-users',
        component: () => import('./layouts/main/Main.vue'),
        children: accessorRoute
      },


        // Redirect to 404 page, if no match found
        {
            path: '*',
            redirect: '/pages/error-404'
        }
    ],
})

router.afterEach(() => {
    // Remove initial loading
    const appLoading = document.getElementById('loading-bg')
    if (appLoading) {
        appLoading.style.display = "none";
    }
})

export default router
