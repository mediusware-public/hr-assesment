import Vue from 'vue'

Vue.filter("formatDate", function (date) {
    return moment(date).format('MMMM Do YYYY');
});

Vue.filter("diffForHumans", function (date) {
    return moment(date).fromNow();
});

Vue.filter("uc_Words", function (str) {
    return _.startCase(_.replace(str, /_/g, ' '));
});

Vue.filter('k_formatter', function (num) {
  return num > 999 ? `${(num / 1000).toFixed(1)}k` : num
})
