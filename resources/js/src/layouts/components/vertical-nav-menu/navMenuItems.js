/*=========================================================================================
 Sidebar Menu
==========================================================================================*/
export default {
  basic: [],
  super_admin: [
    {
      url: "/",
      name: "Home",
      slug: "home",
      icon: "HomeIcon"
    },

    {
      header: 'Company',
      icon: 'PackageIcon',
      i18n: 'Company',
      items: [
        {
          url: null,
          name: "Company",
          slug: "company",
          icon: "PackageIcon",

          submenu: [
            {
              url: "/company",
              name: "Company List",
              slug: "company-list"
            },
            {
              url: "/company/add",
              name: "Add Company",
              slug: "add-company"
            }
          ]
        },

        {
          url: null,
          name: "Membership Plans",
          icon: "CodesandboxIcon",

          submenu: [
            {
              url: "/membership/plans/view-plans",
              name: "Plans",
              slug: "membership-plans"
            },

            {
              url: "/membership/plans/add-plans",
              name: "Add Plans",
              slug: "membership-plans"
            },

            // {
            //   url: null,
            //   name: "Payment Methods",
            //   slug: "membership-plans"
            // },
            // {
            //   url: null,
            //   name: "Transaction Histories",
            //   slug: "membership-plans"
            // },
            // {
            //   url: null,
            //   name: "API Fatturaincloud",
            //   slug: "membership-plans"
            // },
          ]
        },

        /*{
          url: '/membership/payments/stripe',
          name: "Stripe",
          icon: "DatabaseIcon",
          slug: "stripe"
        },*/

        // {
        //   url: '/membership/payments/bank-payments',
        //   name: "Bank Payments",
        //   icon: "DatabaseIcon",
        //   slug: "bank-payments"
        // },
      ]
    },

    // {
    //   header: 'User',
    //   icon: 'PackageIcon',
    //   i18n: 'User',
    //   items: [
    //     {
    //       url: "/user-management",
    //       name: "User Management",
    //       slug: "user",
    //       icon: "UserIcon",
    //       submenu: [
    //         {
    //           url: "/user-management",
    //           name: "User List",
    //           slug: "user"
    //         },
    //         {
    //           url: '/user-management/add',
    //           name: "Add User",
    //           slug: "add-user"
    //         },
    //       ]
    //     },
    //
    //     {
    //       url: "/permission",
    //       name: "Permission",
    //       slug: "permission",
    //       icon: "LockIcon"
    //     },
    //
    //     {
    //       url: "/report",
    //       name: "Report",
    //       slug: "report",
    //       icon: "Edit3Icon"
    //     },
    //
    //     {
    //       url: "/mail",
    //       name: "Mail",
    //       slug: "mail",
    //       icon: "MailIcon"
    //     },
    //
    //     {
    //       url: "/report",
    //       name: "Report",
    //       slug: "report",
    //       icon: "Edit3Icon"
    //     },
    //   ]
    // },

    {
      header: 'Payments',
      icon: 'PackageIcon',
      i18n: 'User',
      items: [
        {
          url: '/membership/payments/bank-payments',
          name: "Bank Payments",
          icon: "DatabaseIcon",
          slug: "bank-payments"
        },
      ]
    },

    // {
    //   header: 'File Manager',
    //   icon: 'PackageIcon',
    //   i18n: 'User',
    //   items: [
    //     {
    //       url: "/media",
    //       name: "File Manager",
    //       slug: "media",
    //       icon: "FolderIcon"
    //     },
    //   ]
    // },

    {
      header: 'Support',
      icon: 'PackageIcon',
      i18n: 'User',
      items: [
        {
          url: null,
          name: "Support",
          icon: "FileTextIcon",
          submenu: [
            {
              url: "/support",
              name: "Ticket List",
              slug: "ticket"
            },
            {
              url: "/support/add",
              name: "Add Ticket",
              slug: "add-ticket"
            },

          ]
        },


        // {
        //   url: "/template/template-builder",
        //   name: "TemplateBuilder",
        //   slug: "template-builder",
        //   icon: "SlackIcon"
        // },
        //
        // {
        //   url: "/schedule",
        //   name: "Schedule",
        //   slug: "schedule",
        //   icon: "CalendarIcon"
        // },

        {
          url: "/help",
          name: "Help",
          slug: "help",
          icon: "LifeBuoyIcon"
        },

        {
          url: "/language",
          name: "Language",
          icon: "GlobeIcon"
        },
      ]
    },

    {
      url: null,
      name: "Setup",
      icon: "SettingsIcon",

      submenu: [
        {
          url: "/settings/payment/add-payment-api",
          name: "PaymentSettings",
          slug: "payment-settings"
        },
        {
          url: "/settings/storage/add-storage-api",
          name: "StorageSettings",
          slug: "storage-settings"
        },

        {
          url: "/settings/zoom/add-zoom-api",
          name: "ZoomSettings",
          slug: "Zoom-settings"
        }
      ]
    }
  ],

  admin: [
    {
      url: "/",
      name: "Dashboard",
      slug: "dashboard",
      icon: "HomeIcon"
    },


    {
      header: 'Report Area',
      icon: 'PackageIcon',
      i18n: 'Report',
      items: [

        {
          url: "/project",
          name: "Project",
          slug: "project",
          icon: "BriefcaseIcon"
        },

        {
          url: "/membership/payments",
          name: "Payment",
          slug: "payment-menu",
          icon: "DollarSignIcon"
        },

        {
          url: "/company-users",
          name: "Users",
          slug: "accessor-menu",
          icon: "BriefcaseIcon"
        },

        {
          url: null,
          name: "Setup",
          slug: "setup",
          icon: "SettingsIcon",
          submenu: [
            {
              url: "/hogan",
              name: "API Hogan Setup",
              slug: "hogan",
              icon: "DiscIcon"
            }
          ]
        },
      ]
    }
  ],

  hr: [
    {
      url: "/",
      name: "Dashboard",
      slug: "dashboard",
      icon: "HomeIcon"
    },

    {
      header: 'Report Area',
      icon: 'PackageIcon',
      i18n: 'Report',
      items: [

        {
          url: "/project",
          name: "Project",
          slug: "project",
          icon: "DollarSignIcon"
        },

        {
          url: "/membership/payments",
          name: "Payment",
          slug: "payment-menu",
          icon: "BriefcaseIcon"
        },

        {
          url: "/company-users",
          name: "Users",
          slug: "accessor-menu",
          icon: "BriefcaseIcon"
        },

        {
          url: null,
          name: "Setup",
          slug: "setup",
          icon: "SettingsIcon",
          submenu: [
            {
              url: "/hogan",
              name: "API Hogan Setup",
              slug: "hogan",
              icon: "DiscIcon"
            }
          ]
        },
      ]
    }
  ],

  user: []
};
