@extends('export-pdf/layouts.app')
@section('content')
  <table class="table table-bordered" id="pdfTableStyle">
    <thead>
    <tr class="table-danger">
      <td>Name</td>
      <td>Email</td>
      <td>Phone</td>
      <td>Address</td>
    </tr>
    </thead>
    <tbody>
    @foreach ($companies as $company)
      <tr>
        <td>{{ $company->name }}</td>
        <td>{{ $company->email }}</td>
        <td>{{ $company->phone }}</td>
        <td>{{ $company->address  }}</td>
      </tr>
    @endforeach
    </tbody>
  </table>
@endsection
