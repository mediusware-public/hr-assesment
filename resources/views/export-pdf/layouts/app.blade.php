<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>PDF Demo in Laravel 7</title>
  <style>
    #pdfTableStyle {
      font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    #pdfTableStyle td, #pdfTableStyle th {
      border: 1px solid #ddd;
      padding: 8px;
    }

    #pdfTableStyle tr:nth-child(even){background-color: #f2f2f2;}

    #pdfTableStyle tr:hover {background-color: #ddd;}

    #pdfTableStyle th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #4CAF50;
      color: white;
    }
  </style>
</head>
<body>
  <div id="app">
    <main>
      @yield('content')
    </main>
  </div>
</body>
</html>
