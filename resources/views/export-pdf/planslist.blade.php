@extends('export-pdf/layouts.app')

@section('content')
  <table class="table table-bordered" id="pdfTableStyle">
    <thead>
    <tr class="table-danger">
      <td><b>Name</b></td>
      <td><b>Price</b></td>
      <td><b>Currency</b></td>
      <td><b>Interval</b></td>
      <td><b>Description</b></td>
    </tr>
    </thead>
    <tbody>
    @foreach ($plans as $plan)
      <tr>
        <td>{{ $plan->name }}</td>
        <td>{{ $plan->price }}</td>
        <td>{{ $plan->currency }}</td>
        <td>{{ $plan->interval }}</td>
        <td>{{ $plan->description }}</td>
      </tr>
    @endforeach
    </tbody>
  </table>
@endsection
