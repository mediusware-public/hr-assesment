@extends('export-pdf/layouts.app')

@section('content')
  <table class="table table-bordered" id="pdfTableStyle">
    <thead>
    <tr class="table-danger">
      <td><b>Date</b></td>
      <td><b>Subject</b></td>
      <td><b>Body</b></td>
    </tr>
    </thead>
    <tbody>
    @foreach ($communications as $communication)
      <tr>
        <td>{{ $communication->created_at }}</td>
        <td>{{ $communication->mail_subject }}</td>
        <td>{!! $communication->mail_body !!}</td>
      </tr>
    @endforeach
    </tbody>
  </table>
@endsection
