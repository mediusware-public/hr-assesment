@extends('export-pdf/layouts.app')
@section('content')
  <table class="table table-bordered" id="pdfTableStyle">
    <thead>
      <tr class="table-danger">
        <td><b>Title</b></td>
        <td><b>Description</b></td>
        <td><b>Status</b></td>
        <td><b>Created By</b></td>
      </tr>
    </thead>
    <tbody>
      @foreach ($tickets as $ticket)
        <tr>
          <td>{{ $ticket->title }}</td>
          <td>{{ $ticket->body }}</td>
          <td>{{ $ticket->status }}</td>
          <td>{{ $ticket->user->name }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
@endsection

