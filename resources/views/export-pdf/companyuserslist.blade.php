@extends('export-pdf/layouts.app')
@section('content')
  <table class="table table-bordered" id="pdfTableStyle">
    <thead>
    <tr class="table-danger">
      <td>Name</td>
      <td>Phone</td>
      <td>Email</td>
      <td>Address</td>
    </tr>
    </thead>
    <tbody>
    @foreach ($companyusers as $user)
      <tr>
        <td>{{ $user->name }}</td>
        <td>{{ $user->phone }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->address  }}</td>
      </tr>
    @endforeach
    </tbody>
  </table>
@endsection
