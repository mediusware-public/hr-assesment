@component('mail::message')
# Subject: {{$mail_data->mail_subject}}

{!! $mail_data->mail_body !!}

{{--@component('mail::button', ['url' => ''])--}}
{{--Button Text--}}
{{--@endcomponent--}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
