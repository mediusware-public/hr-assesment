@component('mail::message')
  # Dear: {{  $mail_data['user']->title .' '. $mail_data['user']->first_name .' '. $mail_data['user']->last_name }}

  Your password reset code is : {!! $mail_data['code'] !!}.

  @component('mail::button',['url'=> url("forget-password/" . $mail_data['token'])])
    Password Reset
  @endcomponent

  Thanks,<br>
  {{ config('app.name') }}
@endcomponent
