<tr>
  <td class="header">
    <a href="{{ $url }}" style="display: inline-block;">
      <img src="https://dev1.galileo-hr.eu/images/logo/logo.png" class="logo" alt="Galileo Logo"
           style="float:left;height: 60px">
      <span style="font-size: 35px">{{ $slot }}</span>
    </a>
  </td>
</tr>
