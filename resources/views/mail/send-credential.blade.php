@component('mail::message')

  ## Hello! {{$user->name}}

  You are receiving this email because we received a password reset request for your account.

  @component('mail::button',['url'=> url("password/reset?token=$user->remember_token")])
    Password Reset
  @endcomponent

@endcomponent
