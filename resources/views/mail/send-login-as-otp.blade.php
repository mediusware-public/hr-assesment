@component('mail::message')

  ## Hello Mr/Mrs {{$otp_data['user']->name}}
  Your OTP is: <strong>{{$otp_data['token']}}</strong>.{{-- This code expires in 5 minutes.--}}

  {{--@component('emails::button',['url'=>'#'])
    Link
  @endcomponent--}}

@endcomponent
