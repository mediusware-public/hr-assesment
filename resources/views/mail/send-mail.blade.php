@component('mail::message')

{{--  ## Hello! {{$user->name}}--}}

  ## Subject: {{$mail_data->mail_subject}}

  {!! $mail_data->mail_body !!}

@endcomponent
